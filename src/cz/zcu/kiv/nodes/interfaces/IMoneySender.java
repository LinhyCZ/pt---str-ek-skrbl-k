package cz.zcu.kiv.nodes.interfaces;

public interface IMoneySender extends IGraphNode {

    /**
     * Odečte počet balíčků s penězi z konta. Pokud na kontě není dostatek peněz, vrátí maximální počet peněz a ty odečte z konta (Zabrání zápornému stavu konta)
     * @param packages Požadované balíčky
     * @return Poskytnuté balíčky
     */
    public int subtractPackages(int packages);

    /**
     * Přidá zadaný počet balíčků s penězi na dané konto
     * @param packages Počet balíčků s penězi
     */
    public void addPackages(int packages);

    /**
     * Metoda zkontroluje zda má instituce dostated pěněz, jestli je může odeslat
     *
     * @param numberOfRequestedBoxes počet požadovaných boxů
     * @return true instituce má dostated pěněz, false instituce peníze už nemá
     */
    public boolean hasMoney(int numberOfRequestedBoxes);

}
