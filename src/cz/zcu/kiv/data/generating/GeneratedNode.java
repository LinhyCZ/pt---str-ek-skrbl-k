package cz.zcu.kiv.data.generating;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GeneratedNode implements Comparable<GeneratedNode> {

    /***
     * Počítadlo
     */
    private static int counter = 0;

    /***
     * Id Instituce
     */
    private int id;

    /***
     * Jméno instituce
     */
    private String name;

    /***
     * Lokace instituce
     */
    private Point point;

    /***
     * Mapa v podobě <ID, distance>>
     */
    private Map<Integer, Float> neighbors;

    /***
     * Typ instituce
     */
    private InstituteType instituteType;

    /***
     * Vzdálenost k hledanému
     */
    private float distanceToSearched;

    /***
     * Vytvoří novou instanci generovaného nodu s id, jmenom, bodem, sousedy a typem instituce
     *
     * @param id                id
     * @param name              jméno
     * @param point             bod
     * @param neighbors         sizsedi
     * @param instituteType     typ instituce
     */
    public GeneratedNode(int id, String name, Point point, Map<Integer, Float> neighbors, InstituteType instituteType) {
        this.id = id;
        this.name = name;
        this.point = point;
        this.neighbors = neighbors;
        this.instituteType = instituteType;
    }

    /***
     * Vytvoří novou instanci henerovaného nodu s jménem, bodem a typem instituce
     *
     * @param name jméno
     * @param point body
     * @param instituteType typ instituce
     */
    public GeneratedNode(String name, Point point, InstituteType instituteType){
        this(counter++, name, point, new HashMap<>(), instituteType);
    }

    /**
     * Vrátí id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /***
     * Vrátí jméno
     *
     * @return jménp
     */
    public String getName() {
        return name;
    }

    /***
     * Vrátí bod generovano nodu
     *
     * @return bod
     */
    public Point getPoint() {
        return point;
    }

    /***
     * Vrátí sousedy v hashmape
     *
     * @return sousedi
     */
    public Map<Integer, Float> getNeighbors() {
        return neighbors;
    }

    /***
     * Vrátí typ instituce
     *
     * @return typ instuce
     */
    public InstituteType getInstituteType() {
        return instituteType;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeneratedNode that = (GeneratedNode) o;
        return id == that.id &&
                name.equals(that.name) &&
                point.equals(that.point) &&
                neighbors.equals(that.neighbors) &&
                instituteType == that.instituteType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, point, neighbors, instituteType);
    }

    @Override
    public String toString() {
        return "GeneratedNode{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", point=" + point +
                ", neighbors=" + neighbors +
                ", instituteType=" + instituteType +
                '}';
    }

    @Override
    public int compareTo(GeneratedNode o) {
        if(o == this) {
            return 0;
        }

        return (int) (this.distanceToSearched - o.distanceToSearched );
    }
}
