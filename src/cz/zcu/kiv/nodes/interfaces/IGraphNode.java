package cz.zcu.kiv.nodes.interfaces;

import cz.zcu.kiv.data.generating.Point;

import java.util.Map;

public interface IGraphNode {

    /**
     * Metoda vrací id vrcholu v grafu
     *
     * @return id vrcholu
     */
    public int getId();

    /**
     * Vrací na základě celočíselné hodnoty souseda a vzdálenost k němu
     *
     * @return vrací mapu sousedů &lt;id, distance&gt;
     */
    public Map<Integer, Float> getNeighbors();

    /**
     * Vratí Point zabalující souřadnice x a y
     *
     * @return vrací bod ve světě simulace
     */
    public Point getPoint();

}
