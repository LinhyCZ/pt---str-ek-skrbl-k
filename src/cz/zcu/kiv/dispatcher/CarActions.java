package cz.zcu.kiv.dispatcher;

/***
 * Enum obsahující všechny akce, které může vozidlo vykonávat (akce ALL slouží pouze pro účely statistiky)
 */
public enum CarActions {
    PASSING, WAITING_UNLOADING, UNLOADING, UNLOADING_DONE, WAITING_LOADING, LOADING, LOADING_DONE, WAITING, ALL;
}
