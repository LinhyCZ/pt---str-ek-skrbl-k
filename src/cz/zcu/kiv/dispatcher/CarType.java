package cz.zcu.kiv.dispatcher;

/***
 * Enum obsahující typy aut, jejich cestovní rychlost a přepravní kapacitu
 */
public enum CarType {
    PANCEROVANY(90, 120),
    OBRNENY(70, 60),
    NAKLADNI(80, 35);

    /***
     * Cestovní rychlost auta
     */
    private int travelSpeed;

    /***
     * Přepravní kapacita
     */
    private int capacity;

    /***
     * Vytvoří nový enum
     * @param travelSpeed Cestovní rychlost auta
     * @param capacity Přepravní kapacita
     */
    CarType(int travelSpeed, int capacity) {
        this.travelSpeed = travelSpeed;
        this.capacity = capacity;
    }

    /***
     * Vrátí cestovní rychlost auta
     * @return Cestovní rychlost auta
     */
    public int getTravelSpeed() {
        return this.travelSpeed;
    }

    /***
     * Vrátí přepravní kapacitu auta
     * @return Přepravní kapacita
     */
    public int getCapacity() {
        return this.capacity;
    }
}
