package cz.zcu.kiv.drawing;

public interface ILogOutput {

    /***
     * Metoda umí zobrazit zprávu na grafický výstup
     *
     * @param message zpráva logu
     */
    void showLogMessage(String message);
}
