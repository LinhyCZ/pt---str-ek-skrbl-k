package cz.zcu.kiv.graphing;

import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;

import java.util.*;

public class Graph {
    /**
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log LOG = Log.getLoger();

    /**
     * Není cesta mezi prvky
     */
    private static final float INFINITY = Float.MAX_VALUE;

    /***
     * Vážená matice sousednosti
     */
    private float [][] matrix;

    /***
     * Vytvoří novou instanci grafu
     *
     * @param cities prvky pro který se má vytvořit graf
     */
    public Graph(IGraphNode[] cities ){
        LOG.log("Probíhá inicializace řetězce finančních institutů", LogLevel.INFO);
        initialize(cities);
        LOG.log("Inicializace finančních institutů byla dokončena", LogLevel.INFO);
    }

    /***
     * Vytvoří váženou matici sousednosti která představuje graf
     *
     * @param cities body grafu
     */
    private void initialize(IGraphNode[] cities) {
        this.matrix = new float[cities.length][cities.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                this.matrix[i][j] = INFINITY;
            }
        }

        for(int i = 0; i < cities.length; i++){
            IGraphNode city = cities[i];
            int id = city.getId();
            Map<Integer, Float> neighbors = city.getNeighbors();
            Iterator it = neighbors.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                int neighborId = (int)pair.getKey();
                float distance   = (float)pair.getValue();
                addRoad(id, neighborId, distance);
            }
        }

    }

    /***
     * Přidá spoj mezi zadanými body a jeho ohodnocení
     *
     * @param i počáteční bod
     * @param j koncový bod
     * @param weight ohodnocení cesty mezi body
     */
    public void addRoad(int i, int j, float weight) {
        matrix[i][j] = weight;
    }

    /***
     * Vrátí všechny sousedy zadaného bodu
     *
     * @param city prvek pro který se mají nalézt všichni sousedi
     * @return sousedi zadaného prvku
     */
    public List<Short> neighbours(int city) {
        List<Short> result = new ArrayList<>();
        for (short i = 0; i < matrix[0].length; i++) {
            if (matrix[city][i] != INFINITY) {
                result.add(i);
            }
        }
        return result;
    }

    /***
     * Zkontroluje zda jsou body sousedními body
     *
     * @param i zkoumaný bod
     * @param j zkoumaný bod
     * @return true jsou sousedi false nejsou sousedi
     */
    public boolean isNeighbour(int i, int j) {
        return matrix[i][j] != INFINITY;
    }

    /**
     * Vrátí graf v podobě vážené matice sousednosti
     *
     * @return vážená matice sousednosti
     */
    public float[][] getMatrix() {
        return this.matrix;
    }
}
