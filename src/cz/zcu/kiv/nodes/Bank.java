package cz.zcu.kiv.nodes;

import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.dispatcher.CarType;
import cz.zcu.kiv.nodes.abstracts.ABigBank;
import cz.zcu.kiv.nodes.interfaces.IRequestable;


import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Map;

public class Bank extends ABigBank implements IRequestable {
    /***
     * Výchozí množství boxů v bance
     */
    private final int DEFAULT_PACKAGES_AMOUNT = 8750 * 2; //Oprava -> Jeden balíček obsahuje pouze 50 tisíc tolarů, proto * 2 (Viz CW -> PT -> Majdišová -> aktuality)

    /***
     * Minimální množství na bance, které by mělo zůstat pro Skrblíky
     */
    private final int DEFAULT_MIN_PACKAGES_AMOUNT = 500;

    /***
     * Vytvoří novou instanci banky.
     *
     * @param id        identifikátor
     * @param name      jméno města kde banka nachází
     * @param point     lokace
     * @param neighbors sousedi banky
     */
    public Bank(int id, String name, Point point, Map<Integer, Float> neighbors) {
        super(id, name, point, neighbors);
    }


    /***
     * Vyresetuje počet boxů na pobočce do výchozího stavu
     */
    public void resetPackages() {
        super.packages = DEFAULT_PACKAGES_AMOUNT;
    }

    /***
     * Metoda kontroluje jestli banka může poslat boxy do regionálních rezidencí a zbylo jí dostatek boxů pro
     * skrblíkovi instituce. Zároveň čím je pokročilejší čas tím se zmenšuje minumální množštví peněz
     * které musí být v bance
     *
     * @return true banka má dostatek boxů false banka už nemá dostatek boxů
     */
    public boolean hasEnoughtPackagesForRegionalResidence(){
        TimeTimer t = TimeTimer.getTimer();
        int midDay = 12;
        int minAmount = 500;
        int temp = (t.getCurrentHour() - midDay);

        if(temp > 0 && t.isWindowOpen()) {
            //za každou hodinu po 12 se odečte minimální požadavek na boxy o 100;
            minAmount = DEFAULT_MIN_PACKAGES_AMOUNT - (temp * 100);
        }


        return ((this.getPackages() - CarType.PANCEROVANY.getCapacity()) > minAmount);
    }

    /***
     * Metoda vykreslí prvek někde na plátně podle jeho souřadnic, které se převedou na souřadnice plátna
     * na základě parametrů panelWidrh a panelHeight
     *
     * @param g2            kreslítko
     * @param panelWidth    šířka plátna
     * @param panelHeight   výška plátna
     */
    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        double posX = ((double) point.x / MAX_X) * panelWidth;
        double posY = ((double) point.y / MAX_Y) * panelHeight;
        double radius = 8;
        double radius2  = radius * 2.5;
        double posX2 = posX - (radius2 / 2);
        double posY2 = posY - (radius2 / 2);

        posX = posX - (radius / 2);
        posY = posY - (radius / 2);

        if(selected){
            Ellipse2D higlight = new Ellipse2D.Double(posX2, posY2, radius2 , radius2);
            g2.setColor(Color.GREEN);
            g2.fill(higlight);
        }



        Ellipse2D elipse2D = new Ellipse2D.Double(posX, posY, radius, radius);
        g2.setColor(Color.RED);
        g2.fill(elipse2D);
    }

    @Override
    public void generateRequest(int hour) {
        /* Hlavní banka nemá requesty */
    }

    @Override
    public void initializeRequests() {
        /* Hlavní banka nemá requesty */
    }

    @Override
    public void sendRequest(int packages) {
        /* Hlavní banka nemá requesty */
    }
}
