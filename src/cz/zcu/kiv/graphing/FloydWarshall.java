package cz.zcu.kiv.graphing;

import cz.zcu.kiv.io.IO;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.logging.Log;

import java.util.LinkedList;
import java.util.Queue;

public class FloydWarshall {
    /**
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log log = Log.getLoger();

    /**
     * Distační matice ze které lze vyčíst vzdálenosti mezi body
     */
    private float [][] dist;

    /**
     * matice z níž lze vyčíst trasy mezi body
     */
    private short [][] next;

    /***
     * Vytvoří novou instanci třádi
     *
     * @param graph graf ve formě matice vážené matice sousednosti
     * @param save  parametr zda se mají trasy po vytvoření uložit
     */
    public FloydWarshall(float[][] graph, boolean save) {
        initialize(graph);
        if(save){
            IO.getInstance().saveAsJson(this, IO.CITIES_PATHS_PATH);
        }

    }

    /***
     * Pomocí algoritmu Floyd Warshalla naleznete nejkratší trasy mezi všemi body v grafu
     *
     * @param dist graf ve formě matice, kde jsou zaznamenány vzdálenosti mezi městy
     */
    private void initialize(float[][] dist) {
        log.log("Probiha planovani optimalnich cest mezi mesty...", LogLevel.NOTICE);

        short[][] next = new short[dist.length][dist[0].length];
        for (int i = 0; i < next.length; i++) {
            for (int j = 0; j < next.length; j++) {
                if (i != j) {
                    next[i][j] = (short) (j + 1);
                }
            }
        }

        for (int k = 0; k < dist.length; k++) {
            System.out.println(k);
            for (int i = 0; i < dist.length; i++) {
                for (int j = 0; j < dist.length; j++) {
                    if (dist[i][k] + dist[k][j] < dist[i][j]) {
                        dist[i][j] = (dist[i][k] + dist[k][j]);
                        next[i][j] = next[i][k];
                    }
                }
            }
        }
        log.log("Planovani dokonceno.", LogLevel.NOTICE);
        this.dist = dist;
        this.next = next;
    }

    /**
     * Vrati trasu do cilove pozice v podobe fronty.
     * Ve fronte neni obsazeny startovni vrchol, Predpoklada se ze z nej bude jakykoliv node vychazet.
     * @param start počáteční bod
     * @param end koncoví bod
     * @return cesta mezi zadanými body
     */
    public Queue<Integer> pathBetweenCities(int start, int end){
        Queue<Integer> path = new LinkedList<>();
        if (start != end) {
            int u = (start + 1);
            int v = (end + 1);
            String pathS = String.format("%d -> %d    %2f     %s", u-1, v-1,
                    dist[start][end], u-1);
            do {
                u =  next[u - 1][v - 1];
                pathS += " -> " + (u-1);
                path.add(u-1);
            } while (u != v);

            log.log(pathS, LogLevel.DEBUG);
        }else{
            log.log("Nelze prepravovat box do sidla ve kterem je aktualne dodavka", LogLevel.ERROR);
        }

        return path;
    }


    /**
     * Vrátí délku trasy mezi dvěma body v grafu
     *
     * @param start počáteční bod
     * @param end koncoví bod
     * @return délka trasy
     */
    public float pathLengthBetweenCities(int start, int end){
        return  dist[start][end];
    }

}
