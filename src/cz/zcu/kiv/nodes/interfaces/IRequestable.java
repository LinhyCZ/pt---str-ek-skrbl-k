package cz.zcu.kiv.nodes.interfaces;

public interface IRequestable extends IGraphNode{

  /***
   *  Vygeneruje nový request dispatcherovi, že pobočka potřebuje boxy.
   *  Podle toho kolik je jich potřeba tolik se jich vygeneruje.
   *
   * @param hour hodina kdy se má request vygenerovat
   */
  void generateRequest(int hour);

  /**
   * Každý den ráno vygeneruje requesty a hodiny v kolik se odešlou
   */
  void initializeRequests();

  /**
   * Zpracuje vyložení boxů z auta v bance
   * @param packages počet boxů ke zpracování
   */
  void addPackages(int packages);

  /***
   * Odešle dispatcherovi reguest o boxy a to je vyšle
   * z nadřazené finační instituce
   *
   * @param packages počet boxů které si instituce žádá
   */
  void sendRequest(int packages);

}
