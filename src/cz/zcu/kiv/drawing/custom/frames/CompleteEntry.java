package cz.zcu.kiv.drawing.custom.frames;

import com.google.gson.annotations.Expose;
import cz.zcu.kiv.dispatcher.Car;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CompleteEntry {
    /***
     * Jméno prvku
     */
    @Expose
    private final String name;

    /***
     * Počet žádostí
     */
    @Expose
    protected int numberOfRequests;

    /***
     * Počet balíčků
     */
    @Expose
    protected int numberOfPackages;

    /***
     * Seznam dalších žádostí
     */
    @Expose
    protected final List<CompleteEntry> requests;

    /***
     * Prvek je poslední
     */
    private final boolean isLeaf;

    /***
     * Seznam aut
     */
    protected final List<Integer> cars = new LinkedList<>();

    /***
     * Konstruktor prvku
     * @param name Jméno prvku
     * @param numberOfRequests Počet žádostí
     * @param numberOfPackages Počet balíčků
     * @param isLeaf Prvek je poslední
     */
    public CompleteEntry(String name, int numberOfRequests, int numberOfPackages, boolean isLeaf) {
        this.name = name;
        this.isLeaf = isLeaf;

        if (!isLeaf) {
            requests = new LinkedList<>();
        } else {
            requests = null;
        }

        this.numberOfRequests = numberOfRequests;
        this.numberOfPackages = numberOfPackages;

    }

    /***
     * Vrátí seznam dalších žádostí
     * @return Seznam dalších žádostí
     */
    public List<CompleteEntry> getRequests() {
        return requests;
    }

    /***
     * Vrátí jestli je prvek koncový
     * @return true = prvek je koncový, false = prvek není koncový
     */
    public boolean isLeaf() {
        return isLeaf;
    }

    /***
     * Vrátí jméno prvku
     * @return jméno prvku
     */
    public String getName() {
        return name;
    }

    /***
     * Vrátí jméno prvku
     * @return jméno prvku
     */
    public String toString() {
        return name;
    }

    /***
     * Přidá auto do seznamu
     * @param car auto
     */
    public void addCar(Car car) {
        int id = car.getID();
        if (!cars.contains(id)) {
            cars.add(id);
        }
    }

    /***
     * Vrátí seznam aut
     * @return seznam aut
     */
    public String outputCarList() {
        String array = Arrays.toString(cars.toArray());
        return array.substring(1, array.length() - 1);
    }
}
