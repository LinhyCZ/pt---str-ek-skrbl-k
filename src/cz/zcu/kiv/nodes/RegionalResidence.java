package cz.zcu.kiv.nodes;

import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.dispatcher.CarType;
import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.dispatcher.MoneyRequest;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.abstracts.ABigBank;
import cz.zcu.kiv.nodes.interfaces.IRequestable;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Map;

public class RegionalResidence extends ABigBank implements IRequestable {
    /***
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Počet kolik si boxů může rezidence nejvíce vyžádat
     */
    private static final int REQUEST_THRESHOLD = CarType.PANCEROVANY.getCapacity();

    /***
     * Počet vyžádaných boxů
     */
    private int requestedPackages = 0;

    private int attemptedPackages = 0;

    /***
     * Maximální kapacita boxů s tolary
     */
    private static final int MAX_CAPACITY = 2500;

    public RegionalResidence(int id, String name, Point point, Map<Integer, Float> neighbors) {
        super(id, name, point, neighbors);
        super.packages = MAX_CAPACITY;
    }


    /***
     * Zkontroluje jestli je potřeba doplnit lokální zásoby a pokud ano,
     * tak vyšle request o boxy do banky.
     */
    public void checkAndSendRequestToBank() {
        //počet plných dodávek o které se bude žádat
        int numberOfFullTrucksToRequest = (RegionalResidence.MAX_CAPACITY - this.getPackages() - this.requestedPackages) / REQUEST_THRESHOLD;

        //Pokud je potřeba odeslat do této pobočky aspon jeden plný truck tak ho vyžádej
        if (numberOfFullTrucksToRequest > 0) {
            MoneyRequest newRequest = new MoneyRequest(this, RegionalResidence.REQUEST_THRESHOLD);
            boolean result = Dispatcher.getDispatcher().newRequest(newRequest);
            if (result) {
                this.requestedPackages += RegionalResidence.REQUEST_THRESHOLD;
            } else {
                this.attemptedPackages += RegionalResidence.REQUEST_THRESHOLD;
            }
        }

        LOG.logSimulation("Rezidence ["+this.name +"] zasílá požadavek do banky o "+(numberOfFullTrucksToRequest * REQUEST_THRESHOLD)+" boxů.",LogLevel.INFO);
    }

    /***
     * Kontroluje zda sídlo potřebuje doplnit boxy;
     *
     * @return true banka potřebuje ještě boxy false nepotřebuje nic
     */
    public boolean needBoxes(){
        int numberOfFullTrucksToRequest = (RegionalResidence.MAX_CAPACITY - this.getPackages() - this.requestedPackages - this.attemptedPackages) / REQUEST_THRESHOLD;
        return (numberOfFullTrucksToRequest > 0);
    }

    public void resetAttemptedPackages() {
        this.attemptedPackages = 0;
    }

    /**
     * Přidá zadaný počet balíčků s penězi na dané konto
     *
     * @param packages Počet balíčků s penězi
     */
    public void addPackages(int packages) {
        this.packages += packages;
        this.requestedPackages -= packages;
    }

    /**
     * @param packages počet boxů které si instituce žádá
     */
    @Override
    public void sendRequest(int packages) {
        this.requestedPackages += packages;
        MoneyRequest newRequest = new MoneyRequest(this, packages);
        Dispatcher.getDispatcher().newRequest(newRequest);
    }

    /***
     * Podle hodiny se zkontrolují vygenerují requesty
     *
     * @param hour hodina kdy se má request vygenerovat
     */
    @Override
    public void generateRequest(int hour) {
        checkAndSendRequestToBank();
    }

    /***
     * Implementace nic nedělá v případě Regionální rezidence. Generují se requesty podle
     * hodinového signálu
     */
    @Override
    public void initializeRequests() {
        //Regionální banky dávají požadavky na základě aktuálního stavu v bance a ne podle hodinového signálu
    }


    /***
     * Metoda vykreslí prvek někde na plátně podle jeho souřadnic, které se převedou na souřadnice plátna
     * na základě parametrů panelWidrh a panelHeight
     *
     * @param g2            kreslítko
     * @param panelWidth    šířka plátna
     * @param panelHeight   výška plátna
     */
    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        double posX = ((double) point.x / MAX_X) * panelWidth;
        double posY = ((double) point.y / MAX_Y) * panelHeight;
        double radius = 5;
        double radius2  = radius * 2.5;
        double posX2 = posX - (radius2 / 2);
        double posY2 = posY - (radius2 / 2);

        posX = posX - (radius / 2);
        posY = posY - (radius / 2);

        if(selected){
            Ellipse2D higlight = new Ellipse2D.Double(posX2, posY2, radius2 , radius2);
            g2.setColor(Color.GREEN);
            g2.fill(higlight);
        }

        g2.setColor(Color.BLUE);
        Ellipse2D elipse2D = new Ellipse2D.Double(posX, posY, radius, radius);
        g2.fill(elipse2D);
    }
}