package cz.zcu.kiv.drawing.custom.frames;

import de.javagl.treetable.AbstractTreeTableModel;
import de.javagl.treetable.JTreeTable;

import javax.swing.*;
import java.awt.*;

public class StatisticsFrame extends JFrame {
    /***
     * Model, podle kterého se statistika vykreslí
     */
    protected AbstractTreeTableModel model;

    /***
     * Instance stromu
     */
    protected JTreeTable tree;


    /***
     * Konstruktor vytvoří okno se stromem podle zadaného modelu
     * @param width šířka okna
     * @param height výška okna
     * @param model Model, podle kterého se statistika vykreslí
     */
    public StatisticsFrame(int width, int height, AbstractTreeTableModel model){
        super();
        JPanel panel = new JPanel(new BorderLayout());

        panel.setSize(new Dimension(width, height));

        //model = ExampleTreeTableModels.createSimple();
        this.tree = new JTreeTable(model);
        this.model = model;

        panel.add(new JScrollPane(tree));

        this.add(panel);

        this.setMaximumSize(new Dimension(width, height));

        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setMinimumSize(new Dimension(width, height));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Complete statistics");
        this.pack();
    }

    /***
     * Vrátí model, podle kterého se statistika vykreslí
     * @return Model, podle kterého se statistika vykreslí
     */
    public AbstractTreeTableModel getModel() {
        return this.model;
    }

    /***
     * Překreslí okno se statistikou
     */
    public void updateTree() {
        this.tree.updateUI();
    }
}