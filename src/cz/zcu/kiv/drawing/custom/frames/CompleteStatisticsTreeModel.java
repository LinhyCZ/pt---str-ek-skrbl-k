package cz.zcu.kiv.drawing.custom.frames;

import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import de.javagl.treetable.AbstractTreeTableModel;
import de.javagl.treetable.TreeTableModel;

public class CompleteStatisticsTreeModel extends AbstractTreeTableModel {
    /***
     * Jména sloupců
     */
    static protected String[] cNames = {"", "Auto", "Počet žádostí", "Počet boxů"};

    /***
     * Typy sloupců
     */
    static protected Class[]  cTypes = { TreeTableModel.class, String.class, String.class, String.class };

    /***
     * Konstruktor modelu
     */
    public CompleteStatisticsTreeModel() {
        super(new CompleteEntry("Banky", 0, 0, false));
    }

    /***
     * Vrátí počet potomků daného prvku
     * @param node daný prvek
     * @return počet potomků
     */
    public int getChildCount(Object node) {
        if (!((CompleteEntry) node).isLeaf()) {
            return ((CompleteEntry) node).getRequests().size();
        }
        return 0;
    }

    /***
     * Vrátí potomka na zadné pozici
     * @param node prvek
     * @param i pozice potomka
     * @return potomek
     */
    public Object getChild(Object node, int i) {
        return ((CompleteEntry) node).getRequests().get(i);
    }

    /***
     * Vrátí jestli je daný prvek koncový
     * @param node zadaný prvek
     * @return true = prvek je koncový, false = prvek není koncový
     */
    public boolean isLeaf(Object node) {
        return ((CompleteEntry) node).isLeaf();
    }

    /***
     * Vrátí počet sloupců
     * @return počet sloupců
     */
    public int getColumnCount() {
        return cNames.length;
    }

    /***
     * Vrátí název daného sloupce
     * @param column pořadí sloupce
     * @return název daného sloupce
     */
    public String getColumnName(int column) {
        return cNames[column];
    }

    /***
     * Vrátí třídu daného sloupce
     * @param column pořadí sloupce
     * @return třída daného sloupce
     */
    public Class getColumnClass(int column) {
        return cTypes[column];
    }

    /***
     * Vrátí hodnotu daného sloupce pro daný prvek
     * @param node prvek
     * @param column pořadí sloupce
     * @return hodnota daného sloupce pro daný prvek
     */
    public Object getValueAt(Object node, int column) {
        switch(column) {
            case 0:
                return node;
            case 1:
                return ((CompleteEntry)node).outputCarList();
            case 2:
                return ((CompleteEntry)node).numberOfRequests;
            case 3:
                return ((CompleteEntry)node).numberOfPackages;
            default:
                System.out.println("Unknown column");
        }

        return null;
    }

    /***
     * Přidá banku do stromu
     * @param institute banka
     */
    public void addBank(AFinancialInstitute institute) {
        ((CompleteEntry)getRoot()).requests.add(new CompleteEntry(institute.getName() + " (ID: " + institute.getId() + ")", 0, 0, false));
    }

    /***
     * Přidá den u dané banky do stromu
     * @param institute banka
     * @param day den
     */
    public void addDay(AFinancialInstitute institute, int day) {
        ((CompleteEntry)getRoot()).requests.get(institute.getId()).requests.add(new CompleteEntry("Den: " + day, 0, 0, false));
    }

    /***
     * Přidá uspokojený požadavek do stromu u dané banky v daný den
     * @param institute banka
     * @param day den
     * @param time čas uspokojení požadavku
     * @param requests počet uspokojených žádostí
     * @param packages počet doručených balíčků
     * @param car auto
     */
    public void addCar(AFinancialInstitute institute, int day, String time, int requests, int packages, Car car) {
        ((CompleteEntry)getRoot()).numberOfRequests += requests;
        ((CompleteEntry)getRoot()).numberOfPackages += packages;

        CompleteEntry instituteEntry = ((CompleteEntry)getRoot()).requests.get(institute.getId());
        instituteEntry.numberOfRequests += requests;
        instituteEntry.numberOfPackages += packages;
        instituteEntry.addCar(car);

        CompleteEntry dayEntry = instituteEntry.requests.get(day - 1);
        dayEntry.numberOfRequests += requests;
        dayEntry.numberOfPackages += packages;
        dayEntry.addCar(car);

        CompleteEntry newEntry = new CompleteEntry("Čas: " + time, requests, packages, true);
        newEntry.addCar(car);
        dayEntry.requests.add(newEntry);
    }


}
