package cz.zcu.kiv;

import cz.zcu.kiv.drawing.BasicDrawing;
import cz.zcu.kiv.graphing.FloydWarshall;
import cz.zcu.kiv.graphing.Graph;
import cz.zcu.kiv.io.IO;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IDrawable;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;
import cz.zcu.kiv.statistics.StatisticsCounter;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {
    /**
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log LOG = Log.getLoger();

    public static void main(String[] args){
        BasicDrawing basicDrawing = BasicDrawing.getInstance();
        //Inicializovat logger a level logování
        Log.setLevel(LogLevel.INFO);
        Log.getLoger().setLoggable(basicDrawing);
        //načíst data a přemapovat data
        AFinancialInstitute[] financialInstitutes = InstituteMapper.getInstance().mapGeneratedNodeToFinancialInstitute(IO.getInstance().loadCities(IO.CITIES_SOURCES_PATH));
        basicDrawing.setCitiesList(financialInstitutes);
        //Inicializovat potřebné pomocné dataové struktury
        IGraphNode[] graphNodes = Arrays.copyOf(financialInstitutes, financialInstitutes.length, IGraphNode[].class);
        Graph g = new Graph(graphNodes);
        FloydWarshall fw = loadPaths(g);
        LOG.log("Inicializace aplikace byla dokončena", LogLevel.INFO);
        //předat nody vizualizaci
        IDrawable[] tmpDrawables = Arrays.copyOf(financialInstitutes, financialInstitutes.length, IDrawable[].class);
        List<IDrawable> drawables = Arrays.asList(tmpDrawables);
        basicDrawing.setNodes(drawables);
        //BasicDrawing basicDrawing = new BasicDrawing(720, 720, drawables);
        //spustit simulaci
        Simulation simulation = new Simulation(fw, financialInstitutes, basicDrawing);
        StatisticsCounter.getInstance().setSimulation(simulation);

        basicDrawing.setSimulation(simulation);
        //kvůli glitchům co se objevovali po přidání nodu
    }

    /***
     * Načte plán všech cest mězi městy
     *
     * @param g neorientovaný graf měst
     * @return vrací naplánovéné nejkratší cesty mezi městy
     */
    private static FloydWarshall loadPaths(Graph g){
        FloydWarshall fw = null;

        LOG.log("Probíhá plánování optimálních tras mezi městy.", LogLevel.INFO);
        if(Files.exists(Paths.get(IO.CITIES_PATHS_PATH))){
            long start = System.currentTimeMillis();
            fw = IO.getInstance().loadPaths(IO.CITIES_PATHS_PATH);
            long end = System.currentTimeMillis();
            System.out.println((end - start)/1000);
        }else {
            fw = new FloydWarshall(g.getMatrix(), false);
        }

        return fw;
    }

}
