package cz.zcu.kiv.nodes;

import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.nodes.abstracts.ALocalBank;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Map;

public class PartnerResidence extends ALocalBank {

    /***
     * Vytvoří novou instanci Partnerské rezidence
     *
     * @param id        identifikátor rezidence
     * @param name      jméno města, kde je rezidence
     * @param point     lokace, kde se rezidence nachází
     * @param neighbors sousední města rezidence
     */
    public PartnerResidence(int id, String name, Point point, Map<Integer, Float> neighbors) {
        super(id, name, point, neighbors);
        super.packages = 0;
    }

    /***
     * Metoda vykreslí prvek někde na plátně podle jeho souřadnic, které se převedou na souřadnice plátna
     * na základě parametrů panelWidrh a panelHeight
     *
     * @param g2            kreslítko
     * @param panelWidth    šířka plátna
     * @param panelHeight   výška plátna
     */
    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        double posX = ((double) point.x / MAX_X) * panelWidth;
        double posY = ((double) point.y / MAX_Y) * panelHeight;
        double radius = 3;
        double radius2  = radius * 2.5;
        double posX2 = posX - (radius2 / 2);
        double posY2 = posY - (radius2 / 2);

        posX = posX - (radius / 2);
        posY = posY - (radius / 2);

        if(selected){
            Ellipse2D higlight = new Ellipse2D.Double(posX2, posY2, radius2 , radius2);
            g2.setColor(Color.GREEN);
            g2.fill(higlight);
        }

        g2.setColor(Color.BLACK);
        Ellipse2D elipse2D = new Ellipse2D.Double(posX, posY, radius, radius);
        g2.fill(elipse2D);
    }
}
