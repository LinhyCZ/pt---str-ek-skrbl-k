package cz.zcu.kiv;



import cz.zcu.kiv.data.generating.GeneratedNode;
import cz.zcu.kiv.data.generating.InstituteType;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.Bank;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.SkrblikResidence;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;


public class InstituteMapper {
    /***
     * Jediná instance třídy
     */
    private static InstituteMapper instance = null;

    /***
     * Instance loggeru pro logování událostí
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Soukromý kontruktor jedináčka
     */
    private InstituteMapper(){}

    /***
     * Statická tovární metoda. Vrací jedinou instanci třídy
     *
     * @return instance Institute mapperu
     */
    public static InstituteMapper getInstance(){
        if(instance == null){
            instance = new InstituteMapper();
        }
        return instance;
    }


    /***
     * Přemapuje instance generatedNode na příslušné instance tříd z package nodes
     *
     * @param generatedNodes vygenerované objekty představující pobočky
     * @return objekty pro simulaci z package nodes
     */
    public AFinancialInstitute[] mapGeneratedNodeToFinancialInstitute(GeneratedNode[] generatedNodes){
        LOG.log("Probíhá přemapování načtěných dat", LogLevel.INFO);
        AFinancialInstitute[] financialInstitutes = new AFinancialInstitute[generatedNodes.length];
        for(int i = 0; i < generatedNodes.length; i++){
            GeneratedNode g = generatedNodes[i];
            if(g.getInstituteType() == InstituteType.BANK){
                financialInstitutes[i] = new Bank(g.getId(), g.getName(), g.getPoint(), g.getNeighbors());
            }else if(g.getInstituteType() == InstituteType.SKRBLIK){
                financialInstitutes[i] = new SkrblikResidence(g.getId(), g.getName(), g.getPoint(), g.getNeighbors());
            }else if(g.getInstituteType() == InstituteType.REGIONAL_RESIDENCE){
                financialInstitutes[i] = new RegionalResidence(g.getId(), g.getName(), g.getPoint(), g.getNeighbors());
            }else {
                financialInstitutes[i] = new PartnerResidence(g.getId(), g.getName(), g.getPoint(), g.getNeighbors());
            }
        }
        LOG.log("Počet přemapovaných institucí: " + financialInstitutes.length, LogLevel.DEBUG);
        LOG.log("Přemapování bylo úspěšně dokončeno", LogLevel.INFO);
        return financialInstitutes;
    }
}
