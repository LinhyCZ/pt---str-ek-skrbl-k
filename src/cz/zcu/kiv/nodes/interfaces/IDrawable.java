package cz.zcu.kiv.nodes.interfaces;

import java.awt.*;

public interface IDrawable {

    /***
     * Metoda vykreslí prvek někde na plátně podle jeho souřadnic, které se převedou na souřadnice plátna
     * na základě parametrů panelWidrh a panelHeight
     *
     * @param g2            kreslítko
     * @param panelWidth    šířka plátna
     * @param panelHeight   výška plátna
     */
    void draw(Graphics2D g2, int panelWidth, int panelHeight);
}
