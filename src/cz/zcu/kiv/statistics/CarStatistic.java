package cz.zcu.kiv.statistics;

import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.dispatcher.MoneyRequest;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;

import java.util.LinkedList;
import java.util.Queue;

public class CarStatistic {
    /***
     * Denní počet požadavků
     */
    private int dailyRequests;


    /***
     * Počet částečných žádostí, které čekají na potvrzení
     */
    private int requestCompletionPending = 0;

    /***
     * Počet balíčků, které čekají na potvrzení
     */
    private int packagesCompletionPending = 0;

    /***
     * Aktuální cesta k požadavku
     */
    private Queue<IGraphNode> currentPath = null;

    /***
     * Auto, pro které je statistika
     */
    private final Car CAR;

    /***
     * Instance hlavního počítadla statistiky
     */
    private final StatisticsCounter COUNTER = StatisticsCounter.getInstance();

    /***
     * Konstruktor nastaví auto, pro které se statistika počítá a předá sebe hlavnímu počítadlu
     * @param car auto
     */
    public CarStatistic(Car car) {
        this.CAR = car;
        COUNTER.newCarStatistic(this);
    }

    /***
     * Částečné uspokojení požadavku, očekává se potvrzení
     * @param request Uspokojená žádost
     */
    public void partialRequest(MoneyRequest request) {
        this.requestCompletionPending++;
        this.packagesCompletionPending += request.getNumberOfBoxes();
    }

    /***
     * Potvrzení částečných požadavků
     * @param request finální požadavek
     */
    public void requestCompleted(MoneyRequest request) {
        COUNTER.requestCompleted(this.CAR, requestCompletionPending, packagesCompletionPending, request);

        this.requestCompletionPending = 0;
        this.packagesCompletionPending = 0;
    }

    /***
     * Akce, když je dokončeno nakládání
     * @param count počet naložených balíčků
     */
    public void loadingCompleted(int count) {
        COUNTER.loadingCompleted(this.CAR, count);
    }

    /***
     * Nová žádost o peníze
     */
    public void newRequest() {
        this.dailyRequests++;
    }

    /***
     * Vrátí denní počet příchozích žádostí
     * @return počet příchozích žádostí za dnešek
     */
    public int getDailyRequests() {
        return dailyRequests;
    }

    /***
     * Vyresetuje denní počet příchozích žádostí
     */
    public void resetDaily() {
        this.dailyRequests = 0;
    }

    /***
     * Vrátí auto, pro které je statistika počítána
     * @return auto, pro které je statistika počítána
     */
    public Car getCar() {
        return this.CAR;
    }

    /***
     * Nastaví aktuální cestu auta
     * @param path aktuální cesta auta
     */
    public void setCurrentPath(Queue<IGraphNode> path) {
        currentPath = (path != null ? new LinkedList<>(path) : null);
    }

    /***
     * Vrátí aktuální cestu auta
     * @return aktuální cesta auta
     */
    public Queue<IGraphNode> getCurrentPath() {
        return currentPath;
    }
}
