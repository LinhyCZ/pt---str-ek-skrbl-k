package cz.zcu.kiv.dispatcher;

import cz.zcu.kiv.data.generating.InstituteType;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.SkrblikResidence;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;

public class MoneyRequest {
    /***
     * Banka, ve které požadavek vznikl
     */
    private AFinancialInstitute origin;

    /***
     * Typ banky, ze které se můžou přivézt peníze
     */
    private InstituteType requiredInstitute;

    /***
     * Auto, kterému byla žádost přiřazena
     */
    private Car assignedTo;

    /***
     * Počet boxů, o které se žádá
     */
    private int numberOfBoxes;

    /***
     * Konstruktor nové žádosti o jeden box peněz
     *
     * @param origin Instituce, ve které požadavek vznikl
     */
    public MoneyRequest(AFinancialInstitute origin) {
        this(origin, 1);
    }

    /***
     * Konstruktor nové žádosti o boxy s penězi
     *
     * @param origin Instituce, ve které požadavek vznikl
     * @param numberOfBoxes Počet boxů o které je zažádáno
     */
    public MoneyRequest(AFinancialInstitute origin, int numberOfBoxes) {
        this.origin = origin;
        this.numberOfBoxes = numberOfBoxes;

        if (origin instanceof SkrblikResidence) {
            requiredInstitute = InstituteType.BANK;
        }
        if (origin instanceof RegionalResidence) {
            requiredInstitute = InstituteType.BANK;
        }
        if (origin instanceof PartnerResidence) {
            requiredInstitute = InstituteType.REGIONAL_RESIDENCE;
        }
    }

    /***
     * Vrátí banku, ve které požadavek vznikl
     * @return Banka, ve které požadavek vznikl
     */
    public AFinancialInstitute getOriginNode() {
        return this.origin;
    }

    /***
     * Vrátí typ banky, ze které se můžou přivézt peníze
     * @return Typ banky, ze které se můžou přivézt peníze
     */
    public InstituteType getRequiredInstitute() {
        return this.requiredInstitute;
    }

    /***
     * Vrátí počet boxů, o které se žádá
     * @return Počet boxů, o které se žádá
     */
    public int getNumberOfBoxes() {
        return this.numberOfBoxes;
    }

    /***
     * Přiřadí auto žádosti
     * @param car auto
     */
    public void setCar(Car car) {
        this.assignedTo = car;
    }

    /***
     * Vrátí auto, kterému byla žádost přiřazena
     * @return Auto, kterému byla žádost přiřazena
     */
    public Car getCar() {
        return this.assignedTo;
    }
}
