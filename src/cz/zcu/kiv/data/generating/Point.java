package cz.zcu.kiv.data.generating;

import java.util.Objects;

public class Point {
    /***
     * Souřadnice x
     */
   public final int x;

    /***
     * souřadnice y
     */
   public final int y;


    /***
     * Vytvoří novou instanci bodu
     *
     * @param x souřadnice x
     * @param y souřadnice y
     */
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
