Seminární práce z předmětu PT na téma efektivního rozvážení peněz mezi bankami. **Semestrální práci jsme vytvořili v dvojici s Radkem Miklánkem. - https://gitlab.com/Miklis**

Banka
* 	- v ventrální části [256, 256]
* 	- max 875M denně (8750 boxů)
* 	

Pěněžní ústavy
* 	- 5000 na celém území 
* 	- min 2km mezi nimi
* 	- 5% Skrblik (250)
*  		- peníze se do nich dováží přímo z centrální banky
* 	- 95% Partnerské instituce (4750) 
* 		- zásobují je regionální sídla
* 	- každý ústav je propojen s 15 nejbližšími ústavy

Regionální sídla
*   - přijí
* 	- 10 regionálních sídel
* 	- 128 km mezi nimi
*	- je spojen s 50 pertnerskými institucemi
* 	- uschova max 2500 boxů

Auta
* auto by si mělo pomatovat svojí destinaci, výchozí pozici a aktuální pozici (přesouvání po cestách grafu v čase)
* auto umí naložit boxy a vyložit boxy (po jedné a čas si bude hlídat sídlo nebo po více a čas si bude hlídat auto 5 min vyložení trvá)
* vrací se prázné boxy, takže by teoreticky stačili dvě proměné prázdné a plné a podle situace se budou dekrementovat či inkrementovat
* aktualizace pozice v čase
* vyložení jedné bedny = dekrementace plné a inkrementace prázdných
* po úplném vyprázdnění všech boxů se auto vrací do své výchozí lokace

Pancéřovaná auta

    - průměrně 90km/h
    - 120 boxů s pěnězi
 
Obrněná auta

    - průměrně 70km/h
    - 60 boxů

Transit

    - průměrně 80 km/h
    - 35 boxů max
