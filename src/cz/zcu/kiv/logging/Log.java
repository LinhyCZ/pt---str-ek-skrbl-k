package cz.zcu.kiv.logging;


import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.drawing.ILogOutput;
import cz.zcu.kiv.io.IO;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
    /**
     * Adresář kam se bude ukládat soubor s logem.
     */
    public static final Path LOG_DIRECTORY = Paths.get("log");

    /**
     * Jediná instance jedináčka
     */
    private static Log onlyInstance = null;

    /**
     * Úroveň logování
     */
    private static int logLevel = LogLevel.DEBUG.getLevel();

    /**
     * Cesta k souboru s logama
     */
    private static final File LOG_PATH = new File("log/log.txt");

    /**
     * Instance pro zápis a čtení ze souboru
     */
    private final IO io = IO.getInstance();

    /**
     * Grafické rozhraní pro logování
     */
    private ILogOutput logOutput;

    /***
     * Simulační čas
     */
    private TimeTimer timer;

    /**
     * soukromý konstruktor jedináčka
     */
    private Log(){ }

    /**
     * Vrátí instanci loggeru
     *
     * @return instance loggeru
     */
    public static Log getLoger(){
        if(onlyInstance == null){
            onlyInstance = new Log();
        }

        return  onlyInstance;
    }

    /**
     * Nastaví do jakého levelu se bude logovat.
     * Podle nastavené úrovně se budou ignorovat nižší
     * úrovně zpráv.
     *
      * @param level úroveň logování
     */
    public static void setLevel(LogLevel level){
        logLevel = level.getLevel();
    }

    /**
     * Přiřadí instanci timeru
     *
     * @param timer timer představující simulační čas.
     */
    public void setTimer(TimeTimer timer){
        if(timer != null){
            this.timer = timer;
        }else {
            throw new NullPointerException();
        }
    }

    /**
     * Nastaví instanci logovatelného výstupu kam se
     * budou zapisovat zprávy zpracovávané loggerem
     *
     * @param logOutput instance grafického rozhraní kam se bude logovat
     */
    public void setLoggable(ILogOutput logOutput){
        this.logOutput = logOutput;
    }

    /***
     * Metoda pro logování událostí v programu.
     * Výpis se zapisuje do souboru a na grafický výstup
     *
     * @param message logovaná zpráva
     * @param ll      úroveň závažnosti zprávy
     */
    public void log(String message, LogLevel ll)  {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        String line = timeStamp + " " + ll.toString() + " - " + message;
        if(ll.getLevel() <= logLevel) {

            if(logOutput != null){
                logOutput.showLogMessage(line);
            }

          //  System.out.println(line);
            io.writeToExistingFile(LOG_PATH, line);
        }
    }

    /***
     * Metoda pro logování simulačních událostí.
     * Výpis se zapisuje do souboru a na grafický výstup
     *
     * @param message logovaná zpráva
     * @param ll      úroveň závažnosti zprávy
     */
    public void logSimulation(String message, LogLevel ll){
        if (timer == null) {
            throw new NullPointerException("Timer is not set");
        }

        String line = timer.getCurrentTimeInStringForm() + " " + ll.toString() + " - " + message;

        if(ll.getLevel() <= logLevel){
            if(logOutput != null){
                logOutput.showLogMessage(line);
            }
       //    System.out.println(line);
            io.writeToExistingFile(LOG_PATH, line);
        }
    }
}
