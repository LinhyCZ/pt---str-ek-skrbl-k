package cz.zcu.kiv.dispatcher;


import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.exceptions.NoPathException;
import cz.zcu.kiv.graphing.FloydWarshall;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IDrawable;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;
import cz.zcu.kiv.nodes.interfaces.IMoneySender;
import cz.zcu.kiv.nodes.interfaces.IRequestable;
import cz.zcu.kiv.statistics.CarStatistic;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.*;

/***
 * Třída Car implementuje auta rozvážející peníze mezi jednotlivými banovními institucemi a veškeré pomocné funkce potřebné pro simulaci aut.
 */
public class Car implements IDrawable {
    /***
     * Čas pro naložení a vyložení jednoho boxu (Vyložit plný, naložit prázdný)
     */
    protected final int TIME_TO_UNLOAD_BOX_AND_LOAD_EMPTY_BOX = 10;

    /***
     * Počítadlo vytvořených vozidel
     */
    private static int Counter = 0;

    /***
     * Instance Loggeru
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Statistika daného auta
     */
    protected final CarStatistic STATS;

    /***
     * ID Auta
     */
    private final int ID;

    /***
     * Počet balíčků, při kterém auto jede doplnit nové boxy/vyprázdnit prázdné boxy
     */
    private final int MONEY_THRESHOLD = 3;

    /***
     * Čas další akce (Průjezd jiným městem, vyložení peněz);
     */
    protected int nextActionTime;

    /***
     * Čas vyjetí z předchozího města
     */
    protected int lastCityTime;

    /***
     * Poslední čas z časovače
     */
    protected int currentTime;

    /***
     * Následující akce vozidla
     */
    protected CarActions nextAction;

    /***
     * Nakládání / Vykládání / Čekání vozidla
     */
    private CarActions nextSignifcantAction;

    /***
     * Následující město, kterým bude vozidlo projíždět
     */
    protected IGraphNode nextCity;

    /***
     * Město, ze kterého vozidlo vyrazilo
     */
    protected IGraphNode currentCity;

    /***
     * Počet přepravek s penězi na autě
     */
    protected int moneyOnBoard;

    /***
     * Počet dostupných přepravek pro další doručení (Počet přepravek na autě - počet přijatých požadavků)
     */
    protected int moneyAvailable;

    /***
     * Typ vozdila - slouží pro určení maximální kapacity vozidla
     */
    protected final CarType type;

    /***
     * Fronta s cestou vozidla
     */
    protected Queue<IGraphNode> path = new LinkedList<>();

    /***
     * Seznam s požadavky
     */
    protected final List<MoneyRequest> requests = new LinkedList<>();

    /***
     * Další žádost, ke které vozdilo jede
     */
    private MoneyRequest nextRequest = null;

    /***
     * Aktuální pozice vozidla
     */
    protected Point location;

    /***
     * Výchozí banka auta
     */
    private final IMoneySender originBank;

    /***
     * Instance FloydWarshall pro určení cesty
     */
    protected final FloydWarshall fw;

    /***
     * Počet naložených prázdných boxů
     */
    private int emptyBoxes = 0;

    /***
     * Počet boxů k vyložení v aktuálním městě
     */
    private int boxesToUnload = 0;

    /***
     * Určuie jestli je auto označené v nějakém seznamu
     * a má se zváraznit na mapě
     */
    private boolean selected = false;



    /***
     * Vytvoří nové auto
     * @param type Typ vozidla
     * @param originBank Banka, ve které auto vzniká
     * @param fw cesty
     */
    public Car(CarType type, IMoneySender originBank, FloydWarshall fw) {
        this.ID = Car.Counter++;
        LOG.logSimulation("Vytvořeno nové auto ID: " + this.ID + ", domovské město: " + originBank, LogLevel.DEBUG);

        this.fw = fw;
        this.type = type;
        this.originBank = originBank;
        this.currentCity = originBank;
        this.nextCity = originBank;
        AFinancialInstitute af = (AFinancialInstitute)originBank;
        this.location = af.getPoint();

        this.STATS = new CarStatistic(this);
    }

    /***
     * Zjistí maximální počet přepravek, které je možné naložit
     * @return počet míst na přepravky s penězi
     */
    public int getSpaceAvailable() {
        return this.type.getCapacity() - this.moneyOnBoard;
    }

    /***
     * Naložení peněz na auto
     *
     * @param moneyToLoad počet peněz, které se na auto mají naložit
     * @return true = naložení se podařilo, false = nelze naložit
     */
    public boolean loadMoney(int moneyToLoad) {
        if (moneyToLoad <= this.getSpaceAvailable()) {
            this.moneyAvailable += moneyToLoad;
            this.moneyOnBoard += moneyToLoad;
            return true;
        }

        return false;
    }

    /***
     * Zjistí pozici vozidla
     *
     * @return aktuální pozice vozidla
     */
    public Point getLocation() {
        CarUtils.updateCarLocation(this);
        return this.location;
    }

    /***
     * Vrátí jestli je auto vybrané
     * @param selected true = vybráno, false = nevybráno
     */
    public void setSelected(boolean selected){
        this.selected = selected;
    }

    /***
     * Vrátí počet peněz, které ještě může rozvézt
     *
     * @return Počet dostupných peněz
     */
    public int getMoneyAvailable() {
        return this.moneyAvailable;
    }

    /***
     * Vrátí počet boxů na autě
     *
     * @return počet boxů v autě
     */
    public int getMoneyOnBoard() {
        return moneyOnBoard;
    }

    /***
     * Zkontroluje, jestli může přijmout žádost a pokud ano, přijme ji, přidá do seznamu žádostí a případně rovnou vyrazí na cestu
     *
     * @param request Žádost o peníze
     * @return true = přijal žádost, false = nepřijal žádost
     */
    public boolean acceptRequest(MoneyRequest request) {
        LOG.logSimulation("Nová žádost pro auto: " + this.ID + " do " + request.getOriginNode() + " (" + request.getOriginNode().getClass().getSimpleName() + "), vzdálenost: " + fw.pathLengthBetweenCities(this.currentCity.getId(), request.getOriginNode().getId()), LogLevel.DEBUG);
        if (request.getNumberOfBoxes() <= this.getMoneyAvailable()) {
            this.moneyAvailable -= request.getNumberOfBoxes();
            requests.add(request);
            request.setCar(this);

            if (this.nextAction == CarActions.WAITING && this.nextActionTime == Integer.MAX_VALUE) {
                this.nextSignifcantAction = CarActions.UNLOADING;
                if (updateNearestPath()) {
                    this.nextAction = CarActions.PASSING;
                } else {
                    this.nextAction = CarActions.UNLOADING;
                }

                this.nextActionTime = currentTime;
            }
            LOG.logSimulation("Přijata! Aktuální akce: " + this.nextAction + " čas:" + this.nextActionTime, LogLevel.DEBUG);

            Dispatcher.getDispatcher().removeLoadedCar(this);

            STATS.newRequest();
            return true;
        }

        return false;
    }


    /***
     * Event času - akce provedená při přijetí hodinového signálu
     * @param time čas v minutách od spuštění aplikace
     */
    public void clockEvent(int time) {
        this.currentTime = time;
        try {
            if (time >= nextActionTime) {
                switch (this.nextAction) {
                    //case LOADING_DONE: - není potřeba, funkce loadingDone() se volá přímo z funkce loadingMoney()

                    case WAITING:
                    case PASSING:
                        carPassing();
                        break;
                    case WAITING_UNLOADING:
                    case UNLOADING:
                        arrivalAtRequest();
                        break;
                    //case UNLOADING_DONE: - není potřeba, funkce unloadingDone() se volá přímo z funkce unloadMoney()
                    case WAITING_LOADING:
                    case LOADING:
                        loadingMoney();
                        break;
                    default:
                        LOG.log("Neošetřená akce auta: " + this.nextAction, LogLevel.EMERGENCY);
                }
            }
        } catch (Exception e) {
            LOG.log("=============", LogLevel.EMERGENCY);
            LOG.log("| EXCEPTION |", LogLevel.EMERGENCY);
            LOG.log("=============", LogLevel.EMERGENCY);
            LOG.logSimulation("Exception data => ID: " + this.ID + ", Poloha: " + this.getLocation() + ", Původní město: " + this.currentCity + " , Následující město: " + this.nextCity + ", Action: " + this.nextAction + ", Action time: " + this.nextActionTime + ", Time: " + TimeTimer.getTimer().getCurrentTimeFromStart()  + ", LastCityTime: " + this.lastCityTime + "počet přepravek: " + this.moneyOnBoard + ", počet dostupných přepravek: " + this.getMoneyAvailable(), LogLevel.EMERGENCY);
            //this.logMoneyQueue(this.requests, "Fronta žádostí: ");
            this.logMoneyList(this.requests, "Fronta žádostí: ", LogLevel.EMERGENCY);
            this.logPathQueue(this.path, "Fronta průjezdů měst: ", LogLevel.EMERGENCY);
            e.printStackTrace();
        }
    }

    /***
     * Provede další krok v cestě
     *
     * @throws NoPathException Pokus o další krok, ale cesta už je prázdná (žádný krok není)
     */
    private void nextCityStep() throws NoPathException {
        if (this.path.isEmpty()) {
            throw new NoPathException("(ID: " + this.ID + ") Path is empty! Origin city: " + this.originBank + ", Current city: " + this.currentCity + ", Next city: " + this.nextCity + ", next request:" + nextRequest.getOriginNode() + ", next action: " + this.nextAction + ", Next significant action: " + this.nextSignifcantAction);
        } else {
            this.currentCity = this.nextCity;
            this.nextCity = this.path.remove();
            this.nextActionTime = currentTime + CarUtils.calculateDistanceTime(fw.pathLengthBetweenCities(currentCity.getId(), nextCity.getId()), this.type);
            this.lastCityTime = currentTime;
        }
    }


    /***
     * Akce pokud auto projíždí městem. Načte další město, do kterého pojede a určí následující akci (Vykládání / Nakládání / Další průjezd městem)
     */
    private void carPassing() {
        //logMoneyQueue(requests, "Požadavky ID: " + this.ID);
        logMoneyList(requests, "Požadavky ID: " + this.ID, LogLevel.DEBUG);
        logPathQueue(path, "Cesta auta ID: " + this.ID, LogLevel.DEBUG);

        try {
            nextCityStep();

            if (this.path.size() == 0 && this.nextSignifcantAction == CarActions.LOADING) {
                this.nextAction = CarActions.LOADING;
            } else if (this.path.size() == 0 && this.nextSignifcantAction == CarActions.UNLOADING) {
                this.nextAction = CarActions.UNLOADING;
            } else {
                this.nextAction = CarActions.PASSING;
            }
        } catch (NoPathException e) {
            e.printStackTrace();
        }
    }

    /***
     * Akce pokud vozidlo přijede do města, ve kterém má vykládat. Zkontroluje, jestli už může vyložit (= dorazil v čase, kdy jsou banky otevřené)
     */
    private void arrivalAtRequest() {
        this.currentCity = this.nextCity;

        if (nextRequest.getOriginNode() instanceof RegionalResidence) {
            if (currentTime > 16 * 60 + (((TimeTimer.getTimer().getCurrentDay() - 1) * 24) * 60)) {
                this.nextAction = CarActions.WAITING_UNLOADING;
                this.nextActionTime = (24 + 8) * 60 + (((TimeTimer.getTimer().getCurrentDay() - 1) * 24 ) * 60);
                LOG.logSimulation("Příjezd vozidla (ID: " + this.ID + " ) po 16 hodině, vykládka proběhne další den v 8:00 (Time: " + this.nextActionTime + ")", LogLevel.NOTICE);
            } else {
                unloadMoney(currentTime, true);
            }
        } else {
            if (currentTime + TIME_TO_UNLOAD_BOX_AND_LOAD_EMPTY_BOX > 16 * 60 + (((TimeTimer.getTimer().getCurrentDay() - 1) * 24) * 60)) {
                this.nextAction = CarActions.WAITING_UNLOADING;
                this.nextActionTime = (24 + 8) * 60 + (((TimeTimer.getTimer().getCurrentDay() - 1) * 24 ) * 60);
                LOG.logSimulation("Příjezd vozidla (ID: " + this.ID + " ) po 16 hodině, vykládka proběhne další den v 8:00 (Time: " + this.nextActionTime + ")", LogLevel.NOTICE);
            } else {
                unloadMoney(currentTime, false);
            }
        }
    }

    /***
     * Akce pokud vozidlo dokončilo vykládání. Pokud má další žádosti, vyrazí na cestu, jinak zkontroluje stav peněz a případně vyrazí do domovské banky naložit peníze
     */
    private void unloadingDone() {
        STATS.requestCompleted(this.nextRequest);
        LOG.logSimulation("Auto["+ this.type.name() +"] (ID: " + this.ID + ") dokončilo vykládku ve městě: " + this.currentCity + " (ID: " + this.currentCity.getId() + ")", LogLevel.INFO);

        if (requests.isEmpty()) {
            //nextRequest = null;

            if (!checkLoading()) {
                this.nextAction = CarActions.WAITING;
                this.nextSignifcantAction = CarActions.WAITING;
                this.nextActionTime = Integer.MAX_VALUE;
            } else {
                assignHomePath();

                try {
                    nextCityStep();

                    if (this.path.size() == 0) {
                        this.nextAction = CarActions.LOADING;
                    } else {
                        this.nextAction = CarActions.PASSING;
                    }

                } catch (NoPathException e) {
                    e.printStackTrace();
                }
            }

            LOG.logSimulation("Auto["+ this.type.name() +"] (ID: " + this.ID + ") Nemá žádné další požadavky. Další akce: " + this.nextAction + ", počet dostupných boxů: " + this.getMoneyAvailable(), LogLevel.DEBUG);
        } else {
            updateNearestPath();
            LOG.logSimulation("ID: " + this.ID + " další požadavek: " + this.nextRequest.getOriginNode(), LogLevel.DEBUG);
            logMoneyList(requests, "ID: " + this.ID + " další žádosti: ", LogLevel.DEBUG);
            logPathQueue(path, "ID: " + this.ID + " další cesta: ", LogLevel.DEBUG);

            this.nextSignifcantAction = CarActions.UNLOADING;

            if (arrivalBeforeClosing(currentTime)) {
                try {
                    nextCityStep();

                    if (this.path.size() == 0) {
                        this.nextAction = CarActions.UNLOADING;
                    } else {
                        this.nextAction = CarActions.PASSING;
                    }
                } catch (NoPathException e) {
                    e.printStackTrace();
                }
            } else {
                this.nextAction = CarActions.WAITING;

                /* Zítřejší den */
                int days = (int) Math.floor(currentTime / (24 * 60)) + 1;

                /* plus osmá hodina (ráno) */
                this.nextActionTime = (days * 24 * 60) + (8 * 60);
            }
        }
    }

    /***
     * Akce při nakládání peněz. Naloží maximální možný počet baíčků z banky a vyloží prázdné balíčky. Případně se vydá na další cestu.
     */
    protected void loadingMoney() {
        int moneyToLoad = this.originBank.subtractPackages(this.getSpaceAvailable());
        this.currentCity = this.originBank;
        this.nextCity = this.currentCity;

        this.loadMoney(moneyToLoad);

        this.emptyBoxes = 0;

        STATS.loadingCompleted(moneyToLoad);

        if (requests.isEmpty()) {
            this.nextAction = CarActions.WAITING;
            this.nextSignifcantAction = CarActions.WAITING;

            this.nextActionTime = Integer.MAX_VALUE;

            Dispatcher.getDispatcher().addLoadedCar(this);

            return;
        }

        //logMoneyQueue(requests, "Požadavky ID: " + this.ID);
        logMoneyList(requests, "Požadavky ID: " + this.ID, LogLevel.DEBUG);
        logPathQueue(path, "Cesta auta ID: " + this.ID, LogLevel.DEBUG);

        updateNearestPath();
        this.nextSignifcantAction = CarActions.UNLOADING;

        carPassing();
    }

    /***
     * Akce vyložení peněz. Nalezne všechny žádosti v daném městě. Pokud nemá přeskočit časování, vykládá po jednom boxu a za 10 minut provede tu samou akci, j
     * inak vyloží všechny boxy najednou a zavolá unloadingDone()
     *
     * @param time aktuální čas simulace
     * @param skipTiming true = vyloží všechny boxy najednou, false = vykládá po jednom boxu.
     */
    private void unloadMoney(int time, boolean skipTiming) {
        LOG.logSimulation("Auto (ID: " + this.ID + ") boxesToUnload: " + this.boxesToUnload, LogLevel.DEBUG);

        if (skipTiming) {
            LOG.logSimulation("Auto["+ this.type.name() +"] (ID: " + this.ID + ") vykládá všechny boxy najednou", LogLevel.DEBUG);
            boxesToUnload = CarUtils.findOtherRequests(this, nextRequest);
            ((IRequestable) this.nextRequest.getOriginNode()).addPackages(boxesToUnload);
            this.moneyOnBoard -= boxesToUnload;
            this.emptyBoxes += boxesToUnload;
            boxesToUnload = 0;

            unloadingDone();
        } else {
            if (boxesToUnload == 0) {
                boxesToUnload = CarUtils.findOtherRequests(this, nextRequest);

                // Kontrola zda při vykládání nepřišla nová žádost - nejdříve načte žádosti, poté opakuje dokud se nevyloží. Poté znovu zkontroluje žádosti.
                // Pokud se při vykládce překročila celá hodina a přišla nová žádost, tak se boxesToUnload znovu načte na nějaké číslo. Pokud ovšem žádost nedorazila,
                // nastaví se akce na UNLOADING_DONE bez dalšího času akce -> provede se příští minutu

                if (boxesToUnload == 0) {
                    unloadingDone();
                    return;
                }
            }

            LOG.logSimulation("Auto["+ this.type.name() +"] (ID: " + this.ID + ") vykládá jeden box, zbývá vyložit: " + this.boxesToUnload, LogLevel.DEBUG);

            boxesToUnload--;

            ((IRequestable) this.nextRequest.getOriginNode()).addPackages(1);
            this.moneyOnBoard -= 1;
            this.emptyBoxes += 1;

            this.nextActionTime = time + TIME_TO_UNLOAD_BOX_AND_LOAD_EMPTY_BOX;
        }
    }

    /***
     * Zkontroluje, jestli je potřeba naložit boxy, nebo vyložit prázdné boxy
     * @return true = je potřeba naložit, false = není potřeba naložit
     */
    private boolean checkLoading() {
        return this.moneyOnBoard <= MONEY_THRESHOLD || this.emptyBoxes >= this.type.getCapacity() - MONEY_THRESHOLD;
    }

    /***
     * Přiřadí vozdilu cestu do domovského města
     */
    private void assignHomePath() {
        this.nextSignifcantAction = CarActions.LOADING;
        nextRequest = null;
        updatePath((AFinancialInstitute) this.originBank);
    }

    /***
     * Vypočítá, jestli vozidlo přijede před zavírací dobou.
     *
     * @param time Aktuální čas
     * @return true = přijede před zavřením, false = přijede po zavření
     */
    private boolean arrivalBeforeClosing(int time) {
        float distance = fw.pathLengthBetweenCities(this.nextCity.getId(), nextRequest.getOriginNode().getId());
        int timeToTravel = CarUtils.calculateDistanceTime(distance, this.type);

        int days = (int) Math.floor(time / (24 * 60));
        int timeToday = time - (days * 24 * 60);

        //Time of closing
        return (timeToday + timeToTravel <= TimeTimer.REQUEST_END);
    }

    /***
     * Aktualizuje cestu mezi aktuálním městem a městem, ve kterém je následující žádost.
     */
    private void updatePath() {
        updatePath(nextRequest.getOriginNode());
    }

    /***
     * Aktualizuje cestu mezi aktuálním městem a specifikovaným městem.
     * @param node Město, do kterého povede cesta
     */
    private void updatePath(AFinancialInstitute node) {
        this.path = CarUtils.getNodePath(fw.pathBetweenCities(this.currentCity.getId(), node.getId()));
        STATS.setCurrentPath(this.path);
    }


    /***
     * Vypíše seznam žádostí o peníze
     * @param q seznam žádostí o peníze
     * @param s úvodní text zobrazený před výpisem
     * @param l úroveň logování
     */
    private void logMoneyList(List<MoneyRequest> q, String s, LogLevel l) {
        String str = s;
        List<MoneyRequest> q2 = new LinkedList<>(q);
        Iterator<MoneyRequest> it = q2.iterator();
        while (it.hasNext()) {
            str += it.next().getOriginNode().toString() + ", ";
        }

        LOG.logSimulation(str, l);
    }

    /***
     * Vypíše aktuální cestu
     * @param q aktuální cesta
     * @param s úvodní text zobrazený před výpisem
     * @param l úroveň logování
     */
    private void logPathQueue(Queue<IGraphNode> q, String s, LogLevel l) {
        String str = s;
        Queue<IGraphNode> q2 = new LinkedList<>(q);
        Iterator<IGraphNode> it = q2.iterator();
        while (it.hasNext()) {
            str += it.next().toString() + ", ";
        }

        LOG.logSimulation(str, l);
    }

    /***
     * Najde nejbližší žádost a pokud pochází z jiného města, než ve kterém se vozidlo nachází, pak akutalizuje cestu vozidla a vrátí true, jinak false
     * @return false = nachází se ve městě žádosti, potřeba vykládat, true = aktualizována cesta, auto může vyrazit
     */
    private boolean updateNearestPath() {
        if (findNearestRequest()) {
            LOG.logSimulation("ID: " + this.ID + " akutalizace nejbližší cesty, zůstávám v: " + this.nextRequest.getOriginNode(), LogLevel.DEBUG);
            return false;
        }

        LOG.logSimulation("ID: " + this.ID + " akutalizace nejbližší cesty, jedu do: " + this.nextRequest.getOriginNode(), LogLevel.DEBUG);
        updatePath();
        return true;
    }

    /***
     * Najde nejbližší žádost - projde celý seznam žádostí a porovnává vzdálenosti mezi městem ve kterým se nachází a městem z iterátoru.
     * Jako další žádost je přiřazena žádost z nejbližšího města
     *
     * @return true = nejbližší žádost je ve městě, ve kterém se nachází, false = je třeba dojet někam jinam
     */
    private boolean findNearestRequest() {
        MoneyRequest nearestRequest = null;
        float nearestDistance = Float.MAX_VALUE;
        Iterator<MoneyRequest> it = requests.iterator();
        while (it.hasNext()) {
            MoneyRequest nextMoneyRequest = it.next();
            if (this.nextRequest != null && this.nextRequest.getOriginNode() == nextMoneyRequest.getOriginNode()) {
                return true;
            }

            float distance = fw.pathLengthBetweenCities(this.currentCity.getId(), nextMoneyRequest.getOriginNode().getId());
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestRequest = nextMoneyRequest;
            }
        }

        LOG.logSimulation("Žádost: " + nearestRequest.getOriginNode() + " hodnota nearestDistance: " + nearestDistance, LogLevel.DEBUG);
        this.nextRequest = nearestRequest;
        return false;
    }

    /***
     * Vrátí typ vozidla
     * @return typ vozidla
     */
    public CarType getType() {
        return this.type;
    }

    /***
     * Vrátí počet boxů, které auto veze
     * @return počet boxů, které auto veze
     */
    public int getTotalBoxes() {
        return this.moneyOnBoard;
    }

    /***
     * Vrátí další akci vozdila
     * @return další akce vozidla
     */
    public CarActions getAction() {
        return this.nextAction;
    }

    /***
     * Vrátí ID vozidla
     * @return ID vozidla
     */
    public int getID(){
        return this.ID;
    }

    /***
     * Vrátí další město, do kterého auto jede
     * @return další město, do kterého auto jede
     */
    public AFinancialInstitute getNextCity(){
        return (AFinancialInstitute) nextCity;
    }

    /***
     * Vrátí město, ve kterém se auto nachází
     * @return město, ve kterém se auto nachází
     */
    public AFinancialInstitute getCurrentCity(){
        return (AFinancialInstitute) currentCity;
    }

    /***
     * Vrátí počet prázdných boxů
     * @return Počet prázdných boxů
     */
    public int getEmptyBoxes(){
        return emptyBoxes;
    }

    /***
     * Vykreslí auto na obrazovce
     *
     * @param g2            kreslítko
     * @param panelWidth    šířka plátna
     * @param panelHeight   výška plátna
     */
    @Override
    public void draw(Graphics2D g2, int panelWidth, int panelHeight) {
        CarUtils.updateCarLocation(this);
        double posX = ((double) location.x / AFinancialInstitute.MAX_X) * panelWidth;
        double posY = ((double) location.y / AFinancialInstitute.MAX_Y) * panelHeight;
        double radius = 3;
        double radius2  = radius * 3;
        double posX2 = posX - (radius2 / 2);
        double posY2 = posY - (radius2 / 2);

        posX = posX - (radius / 2);
        posY = posY - (radius / 2);

        if(selected){
            Ellipse2D higlight = new Ellipse2D.Double(posX2, posY2, radius2 , radius2);
            g2.setColor(Color.GREEN);
            g2.fill(higlight);
        }

        g2.setColor(Color.ORANGE);
        Ellipse2D ellipse2D = new Ellipse2D.Double(posX, posY, radius, radius);
        g2.fill(ellipse2D);
    }

    /***
     * Vrátí banku, ze které auto pochází
     * @return banka, ze které auto pochází
     */
    public IMoneySender getOriginBank() {
        return this.originBank;
    }

    /***
     * Vrátí, jestli auto jede nakládat
     * @return true = jede nakládat, false = nejede nakládat
     */
    public boolean isHeadingToLoading() {
        return (this.nextSignifcantAction == CarActions.LOADING);
    }

    /***
     * Vrátí ID auta ve formátu string
     * @return ID auta ve formátu string
     */
    @Override
    public String toString() {
        return "" + this.ID;
    }

    /***
     * Vrátí statistiku daného auta
     * @return statistika daného auta
     */
    public CarStatistic getStats() {
        return STATS;
    }
}
