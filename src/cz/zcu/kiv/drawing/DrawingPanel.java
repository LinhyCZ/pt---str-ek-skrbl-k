package cz.zcu.kiv.drawing;

import cz.zcu.kiv.nodes.interfaces.IDrawable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class DrawingPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * Vykreslovaná města
	 */
	private List<IDrawable> generatedNodes;

	/**
	 * Vykreslovaná auta
	 */
	private final List<IDrawable> cars;

	/**
	 * Vytvoří novou instanci panelu
	 */
	public DrawingPanel() {
		super();
		this.cars = new ArrayList<>();
	}

	/**
	 * Nastaví seznam měst který se má vykreslovat
	 *
	 * @param drawables vykreslitelné prvky
	 */
	public void setGeneratedNodes(List<IDrawable> drawables){
		this.generatedNodes = drawables;
	}

	/**
	 * Přidá auto které se má vykreslovat
	 *
	 * @param car auto k vykreslení
	 */
	public void addCar(IDrawable car){
		cars.add(car);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


		if(generatedNodes != null){

		    g2.translate(10, 10);
			drawNodes(g2, this.getWidth() - 20, this.getHeight() - 20);
		}

	}

	/***
	 * Vykreslí všechny vykreslitelné nody
	 *
	 * @param g2	kreslítko
	 * @param width	šířka plátna
	 * @param height výska plátna
	 */
	private void drawNodes(Graphics2D g2, int width, int height) {
		generatedNodes.forEach(n -> n.draw(g2, width, height));
		cars.forEach(c -> c.draw(g2, width, height));
	}


}
