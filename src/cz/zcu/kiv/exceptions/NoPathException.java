package cz.zcu.kiv.exceptions;

/***
 * Exception, pokud auto nemá kam jet, ale daná akce mu to přikazuje
 */
public class NoPathException extends Exception {
    public NoPathException(String errorMessage) {
        super(errorMessage);
    }
}
