package cz.zcu.kiv;

import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.drawing.BasicDrawing;
import cz.zcu.kiv.graphing.FloydWarshall;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.Bank;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.SkrblikResidence;
import cz.zcu.kiv.nodes.abstracts.ABigBank;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IRequestable;
import cz.zcu.kiv.statistics.StatisticsCounter;

import javax.swing.Timer;
import java.util.*;

public class Simulation {
    /**
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Počet dní po které bude simulace probíhat
     */
    private final static int daysOfSimulation = 7;

    /***
     * Proměnná zda už simulace byla ukončena
     */
    private boolean simulationEnded = false;

    /**
     * Instince simulačního času
     */
    private final TimeTimer timeTimer;

    /***
     * Instance pro grafický výstup do okna
     */
    private final BasicDrawing basicDrawing;

    /**
     * Dispatcher
     */
    private final Dispatcher dispatcher;

    /***
     * finanční instituty
     */
    private final AFinancialInstitute[] aFinancialInstitutes;

    /**
     * ragionální pobočky
     */
    private final List<ABigBank> regialResidencesAndBank;

    /***
     * Timer puštěný přes "den" s 30 snímky za sekundu
     */
    private final Timer timer = new Timer(1000 / 30, e -> updateSimulation());

    /***
     * noční timer s nastavením na 200 snímků za sekundu
     */
    private final Timer fftimer = new Timer(1000 / 200, e -> updateSimulation());

    /**
     * Vytvoří novou instanci simulace, kdy je potřeba simulaci přiřadit finanční instituty, cesty mezi nimi
     * a správce okna na který jde grafický výstup.
     *
     * @param floydWarshall         cesty mezi instituy
     * @param aFinancialInstitutes  finanční instituy
     * @param basicDrawing          grafický výstup
     */
    public Simulation(FloydWarshall floydWarshall, AFinancialInstitute[] aFinancialInstitutes, BasicDrawing basicDrawing) {
        this.timeTimer = TimeTimer.getTimer();
        this.dispatcher = Dispatcher.getDispatcher();
        this.basicDrawing = basicDrawing;
        dispatcher.setFloydWarshall(floydWarshall);
        this.aFinancialInstitutes = aFinancialInstitutes;
        Dispatcher.aFinancialInstitutes = aFinancialInstitutes;
        dispatcher.setAFinancialInstitutes(aFinancialInstitutes);
        StatisticsCounter.getInstance().initializeCompleteStatisticsFrame();
        StatisticsCounter.getInstance().initializeCarStatisticsFrame();
        this.regialResidencesAndBank = this.filterABigBank();
        LOG.setTimer(timeTimer);
    }

    /**
     * Spustí nebo restartuje simulaci
     */
    public void startSimulation(){
        if (timeTimer.getCurrentHour() >= 16 || timeTimer.getCurrentHour() <= 8) {
            fftimer.start();
        } else {
            timer.start();
        }
    }

    /**
     * Pozastaví simulaci
     */
    public void pauseSimulation(){
        timer.stop();
        fftimer.stop();
    }


    /***
     * Aktualizace simulace
     */
    private void updateSimulation() {
        LOG.logSimulation("", LogLevel.DEBUG);
        //začátek nového dne
        if (!initNewDay()) {
            return;
        }

        //průbězné odesílání requestů
        sendNewRequests();

        //zrychlení simulace
        trigerFF();

        //hodinový výpis
        showLogMessages();

        //aktualizovat odeslané tracky
        dispatcher.refreshCars(timeTimer.getCurrentTimeFromStart());

        //posunutí simulace o minutu dopředu
        timeTimer.refreshTime();
        //přepsání simulačního času v okně
        basicDrawing.setTimeLabel(timeTimer);

        //aktualizace grafického výstupu
        basicDrawing.repaintAll();
    }


    /***
     * Vyfiltruje regionální rezidence ze seznamu všech finančních
     * instituů a vrátí je.
     *
     * @return list regionálních sídel
     */
    private List<ABigBank> filterABigBank() {
        List<ABigBank> aBigBanks = new ArrayList<>();
        for(AFinancialInstitute afi : aFinancialInstitutes){
            if(afi instanceof ABigBank){
                aBigBanks.add((ABigBank) afi);
            }
        }
        return aBigBanks;
    }

    /***
     * Vždy na začátku dne vyresetuje peníze u banky a nageneruje požadavky na další den, vynuluje denní statistiky, vypíše statistiky předchozího dne
     */
    private boolean initNewDay() {
        if (TimeTimer.DAY_START == timeTimer.getCurrentTime()) {
            if (timeTimer.getCurrentDay() == Simulation.daysOfSimulation + 1 && !simulationEnded) {
                StatisticsCounter.getInstance().endOfSimulationSummary();
                fftimer.stop();
                timer.stop();
                simulationEnded = true;
                this.basicDrawing.reenableStartButton();
                return false;
            }

            StatisticsCounter.getInstance().newDay();
            for (AFinancialInstitute fin : aFinancialInstitutes) {
                //vyresetovat boxy u banky
                if (fin instanceof Bank) {
                    ((Bank) fin).resetPackages();
                }
                //nageneruje nové požadavky u partnerských institucí a skrblíka
                if (fin instanceof PartnerResidence || fin instanceof SkrblikResidence) {
                    ((IRequestable) fin).initializeRequests();
                }
            }
        }

        return true;
    }

    /**
     * Pokud je otevřené okno pro requesty
     * tak projde všechny finanční instituy, které mohou
     * žádat peníze a vygeneruje příslušné requesty pokud je třeba
     */
    private void sendNewRequests() {
        if (timeTimer.isWindowOpen() && timeTimer.isHour()) {
            for (AFinancialInstitute fin : aFinancialInstitutes) {
                if (fin instanceof SkrblikResidence || fin instanceof PartnerResidence) {
                    ((IRequestable) fin).generateRequest(timeTimer.getCurrentHour());
                }
            }
            //zádost o peníze regionálních sídel
            sendNewRequestsFromRegionalResidences();
        }
    }

    /***
     * Metoda která zajisťuje doplňování zásob regionálním pobočkám
     */
    private void sendNewRequestsFromRegionalResidences() {
        Bank bank = (Bank) Arrays.stream(aFinancialInstitutes).filter(a -> a instanceof Bank).toArray()[0];
        boolean regionalResidenceNeedBoxes = true;
        //jestli má banka dost peněz a zároveň aspoň jedna regionální residence chce prachy
        while (bank.hasEnoughtPackagesForRegionalResidence() && regionalResidenceNeedBoxes){
            regionalResidenceNeedBoxes = false;
            for (AFinancialInstitute fin : aFinancialInstitutes) {
                if (fin instanceof RegionalResidence) {
                    RegionalResidence r = (RegionalResidence) fin;
                    r.checkAndSendRequestToBank();
                    if(r.needBoxes()){
                        regionalResidenceNeedBoxes = true;
                    }
                }
            }
        }

        for (AFinancialInstitute fin : aFinancialInstitutes) {
            if (fin instanceof RegionalResidence) {
                ((RegionalResidence) fin).resetAttemptedPackages();
            }
        }
    }

    /***
     * Zrychlení nebo zpomalení simulace
     */
    private void trigerFF() {
        if (timeTimer.isHour() && timeTimer.getCurrentHour() == 16) {
            startff();
        }

        if (timeTimer.isHour() && timeTimer.getCurrentHour() == 8) {
            stopff();
        }
    }

    /**
     * Zrychlení simulace
     */
    private void startff() {
        timer.stop();
        fftimer.start();
    }

    /**
     * Zpomalení simulace
     */
    private void stopff() {
        timer.start();
        fftimer.stop();
    }

    /***
     * Hodinový výpis
     */
    public void showLogMessages() {
        if (timeTimer.isHour()) {
            StatisticsCounter.getInstance().showHourSummary();
        }
    }

    /***
     * Vrátí seznam regionálních rezidencí s bankou
     * @return seznam regionálních rezidencí s bankou
     */
    public List<ABigBank> getRegialResidencesAndBank() {
        return regialResidencesAndBank;
    }
}
