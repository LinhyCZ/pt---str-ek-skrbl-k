package cz.zcu.kiv.logging;

/***
 * Enum obsahující úrovně logavání
 */
public enum LogLevel {
    EMERGENCY(0), ALERT(1), CRITICAL(2), ERROR(3), WARNING(4),NOTICE(5) ,INFO(6) ,DEBUG(7);

    /**
     * ID levelu
     */
    private int logLevel;

    /***
     * Vytvoří novou instaci LogLevelu
     *
     * @param level id levelu
     */
    LogLevel(int level){
        this.logLevel = level;
    }

    /**
     * Vrátí id levelu
     *
     * @return id levelu
     */
    public int getLevel(){
        return logLevel;
    }

    /***
     * Sestaví řetězec náznu instance enumu
     *
     * @return řetězec obsahující název instance
     */
    @Override
    public String toString() {
        switch (this) {
            case INFO:
                return "[INFO]";
            case DEBUG:
                return "[DEBUG]";
            case ERROR:
                return "[ERROR]";
            case WARNING:
                return "[WARNING]";
            case ALERT:
                return "[ALERT]";
            case NOTICE:
                return "[NOTICE]";
            case CRITICAL:
                return "[CRITICAL]";
            case EMERGENCY:
                return "[EMERGENCY]";
            default:
                return "non specified";
        }
    }
}
