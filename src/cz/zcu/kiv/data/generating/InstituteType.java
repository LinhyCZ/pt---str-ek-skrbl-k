package cz.zcu.kiv.data.generating;

/***
 * Enum obsahující typy institucí
 */
public enum InstituteType {
    BANK, SKRBLIK, PARTNER_INSTITUTE, REGIONAL_RESIDENCE;
}
