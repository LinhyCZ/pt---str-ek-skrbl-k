package cz.zcu.kiv.drawing.custom.frames;

import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.drawing.BasicDrawing;
import cz.zcu.kiv.nodes.Bank;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.SkrblikResidence;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IRequestable;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class InstitutesFrame extends JFrame {

    /***
     * Jediná instance jedináčka
     */
    private static InstitutesFrame instance = null;

    /***
     * Number model sponeru pro regionální rezidence
     */
    private final SpinnerNumberModel regionalResidenceSpinner = new SpinnerNumberModel(60, 1, 120, 10);

    /***
     * number model spineru pro malé instituce
     */
    private final SpinnerNumberModel residenceSpinner = new SpinnerNumberModel(3, 1,  6, 1);

    /***
     * Instance timeru představující čas simulace
     */
    private final TimeTimer time = TimeTimer.getTimer();

    /***
     * Levý panel
     */
    private final JPanel left;

    /***
     * Střední panel (pravý)
     */
    private final JPanel center;

    /***
     * JList všech institucí
     */
    private JList<String> instituteList;

    /**
     * Textové pole s informací o id instituce
     */
    private JTextField idTF;

    /***
     * Textové pole s informací o jméně instituce
     */
    private JTextField nameTF;

    /***
     * Textové pole s informací o typu instituce
     */
    private JTextField typeTF;

    /***
     * Textové pole s informací o počtu peněz
     */
    private JTextField moneyTF;

    /**
     * Spinner pro množství boxů které si instituce vyžádá
     */
    private JSpinner spinner;

    /***
     * Tlačítko pro odeslání requestů
     */
    private JButton reqPackButton;

    /***
     * Label pro text field o penězích
     */
    private JLabel moneyLabel;

    /***
     * Vybraná instituce v Jlistu
     */
    private AFinancialInstitute selectedInstitute;

    /***
     * Hashovací mapa institucí
     */
    private Map<String, AFinancialInstitute> institutes;

    /***
     * Vytvoří novou instanci framu insitucí o dané vyšce a šířce
     *
     * @param width šířka okna
     * @param height výška okna
     */
    private InstitutesFrame(int width, int height){
        super();
        JPanel panel = new JPanel(new BorderLayout());
        this.left = new JPanel();
        this.center = new JPanel();

        panel.setSize(new Dimension(width, height));

        this.add(panel);
        panel.add(left, BorderLayout.WEST);
        panel.add(center, BorderLayout.CENTER);

        this.setMaximumSize(new Dimension(width, height));

        createLeftPanel();
        createRightPanel();

        this.setVisible(false);
        this.setMinimumSize(new Dimension(width, height));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Institutes info");

        this.setLocationRelativeTo(null);
        this.pack();
    }

    /***
     * Vratí jedinou instanci jedináčka
     *
     * @return instance
     */
    public static InstitutesFrame getInstance(){
        if(instance == null){
            instance = new InstitutesFrame(BasicDrawing.ADDITIONAL_FRAME_WIDTH, BasicDrawing.ADDITIONAL_FRAME_HEIGHT);
        }
        return instance;
    }

    /***
     * Zobraz okno
     */
    public void setVisible(){
        this.setVisible(true);
    }

    /**
     * Přidá data s institucemi do JListu
     *
     * @param institutes instituce
     */
    public void addDataToJList(AFinancialInstitute[] institutes){
        String[] institutesNames = new String[institutes.length];
        this.institutes = new HashMap<>();

        for(int i = 0; i < institutes.length; i++){
            String name = institutes[i].getName();
            institutesNames[i] = name;
            this.institutes.put(name, institutes[i]);
        }

        instituteList.setListData(institutesNames);
    }

    /***
     * Obnoví pravou část okno podle aktuýlně vybraného prvku
     */
    public void refreshInfo(){

         // Nějaký prvek je vybraný, tak změn jeho údaje
        if(instituteList.getSelectedValue() != null){
            changeFields();
        }


        //je zavřené okno kdy je možné zasílat requesty tak zablokuj příslušné prvky
        if(!time.isWindowOpen() && reqPackButton.isEnabled()) {
            reqPackButton.setEnabled(false);
        }
        //pokud je něco vybráno a je otevřené okno pro požadavky tak povol příslušné prvky
        if(!instituteList.isSelectionEmpty() && !reqPackButton.isEnabled() && time.isWindowOpen()){
            reqPackButton.setEnabled(true);
        }

        this.center.repaint();
    }

    /***
     * Vytvoří levý panel s JListem
     */
    private void createLeftPanel(){
        this.left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

        JLabel cityLabel = new JLabel("Seznam poboček");
        instituteList = new JList<>();

        instituteList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        instituteList.setLayoutOrientation(JList.VERTICAL);
        instituteList.setVisibleRowCount(-1);
        instituteList.addListSelectionListener(event -> changeFields());

        cityLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JScrollPane pane = new JScrollPane();
        pane.setViewportView(instituteList);

        this.left.add(cityLabel);
        this.left.add(pane);
    }

    /***
     * Metoda zajišťuje akci kdy se mění vybraný prvek JListu
     */
    private void changeFields() {
        String instituteName = instituteList.getSelectedValue();
        //žádný prvek není vybraný tak vše vynuluj
        if(instituteList.getSelectedIndex() == -1){
            idTF.setText("");
            nameTF.setText("");
            moneyTF.setText("");
            setResidenceTypeInfo("", false);
            if(selectedInstitute != null){
                selectedInstitute.setSelected(false);
                BasicDrawing.getInstance().rapaintMainPanel();
            }
            return;
        }

        //pokud je vybraná nějká instituce tak je zvýrazni na mapě
        if(selectedInstitute != null){
            this.selectedInstitute.setSelected(false);
        }

        this.selectedInstitute = institutes.get(instituteName);

        this.selectedInstitute.setSelected(true);

        idTF.setText(selectedInstitute.getId() + "");
        nameTF.setText(selectedInstitute.getName());


        //rozhodnutí o jakou instituci se jedná
        if(selectedInstitute instanceof Bank){
            setResidenceTypeInfo("Bank", false);
            moneyTF.setText((selectedInstitute.getPackages() * 100_000) + " tolarů (" + selectedInstitute.getPackages() + " boxů)");
            moneyLabel.setText("Money");
        }else if(selectedInstitute instanceof RegionalResidence){
            moneyTF.setText((selectedInstitute.getPackages() * 100_000) + " tolarů (" + selectedInstitute.getPackages() + " boxů)");
            spinner.setModel(regionalResidenceSpinner);
            moneyLabel.setText("Money");
            setResidenceTypeInfo("Regional residence", true);
        }else if(selectedInstitute instanceof PartnerResidence){
            moneyTF.setText((Dispatcher.getDispatcher().getNumberOfBankRequest(selectedInstitute) * 100_000) + " tolarů (" + Dispatcher.getDispatcher().getNumberOfBankRequest(selectedInstitute) + " boxů)");
            setResidenceTypeInfo("Partner residence", true);
            moneyLabel.setText("Requested money (sum)");
            spinner.setModel(residenceSpinner);
        }else if(selectedInstitute instanceof SkrblikResidence){
            moneyTF.setText((Dispatcher.getDispatcher().getNumberOfBankRequest(selectedInstitute) * 100_000) + " tolarů (" + Dispatcher.getDispatcher().getNumberOfBankRequest(selectedInstitute) + " boxů)");
            setResidenceTypeInfo("Skrblik residence", true);
            moneyLabel.setText("Requested money (sum)");
            spinner.setModel(residenceSpinner);
        }
        //repaint kvůli vybrání instituce
        BasicDrawing.getInstance().rapaintMainPanel();
    }

    /***
     * Nastaví text boxy na o typu rezidence a povolí nebo zakáže prvky
     * pro requesty
     *
     * @param residenceType typ rezidence
     * @param enableRequest povolit requesty
     */
    private void setResidenceTypeInfo(String residenceType, boolean enableRequest){
        typeTF.setText(residenceType);
        spinner.setEnabled(enableRequest);
        if(!time.isWindowOpen()){
            reqPackButton.setEnabled(false);
        }else {
            reqPackButton.setEnabled(enableRequest);
        }

    }

    /***
     * Vytvoří pravý panel s text boxy
     */
    private void createRightPanel(){
        GridLayout gl = new GridLayout(5, 2);
        this.center.setLayout(gl);

        gl.setVgap(15);
        gl.setHgap(10);

        JLabel idLabel = new JLabel("ID:");
        JLabel nameLabel = new JLabel("Name:");
        JLabel typeLabel = new JLabel("Type:");
        moneyLabel = new JLabel("Money:");


        idTF = new JTextField();
        nameTF = new JTextField();
        typeTF = new JTextField();
        moneyTF = new JTextField();

        idTF.setEditable(false);
        nameTF.setEditable(false);
        typeTF.setEditable(false);
        moneyTF.setEditable(false);


        spinner = new JSpinner(new SpinnerNumberModel(3,0,6,1));
        reqPackButton = new JButton("Send request");

        this.center.add(idLabel);
        this.center.add(idTF);

        this.center.add(nameLabel);
        this.center.add(nameTF);

        this.center.add(typeLabel);
        this.center.add(typeTF);

        this.center.add(moneyLabel);
        this.center.add(moneyTF);

        this.center.add(spinner);
        this.center.add(reqPackButton);
        
        reqPackButton.addActionListener(e -> requestPackages());
    }

    /***
     * Započně událost kdy se odešle nový požadavek o boxy
     */
    private void requestPackages() {
        ((IRequestable)this.selectedInstitute).sendRequest((int)spinner.getValue());
    }
}
