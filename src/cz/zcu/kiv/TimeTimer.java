package cz.zcu.kiv;

public class TimeTimer {
    /***
     * Jediná instance TimeTimeru
     */
    private static TimeTimer instance = null;

    /**
     * Začátek okna, kdy mohou instituce posílat své požadavky na boxy.
     */
    public static final int REQUEST_START = 60 * 8;

    /**
     * Konec okna, kdy mohou instituce posílat své požadavky na boxy.
     */
    public static final int REQUEST_END = 60 * 16;

    /**
     * Púlnoc (0:00)
     */
    public static final int DAY_START = 0;

    /**
     * Minuta před půlnocí (23:59)
     */
    private final int DAY_END = (23 * 60) + 59;

    /**
     * Aktuální čas simulace
     */
    private int currentTime;

    /**
     * Aktuální čas od počátku simulace
     */
    private int currentTimeFromStart;

    /**
     * Aktuální den simulace
     */
    private int currentDay;

    /**
     * Nedělání nic jiného než tvorbu instance a nastavení výchozích hodnot.
     */
    public TimeTimer() {
        start();
    }

    /**
     * Vrátí aktuální den simulace
     *
     * @return aktuální den simulace
     */
    public int getCurrentDay(){
        return currentDay;
    }

    /**
     * aktuální denní čas v minutách
     *
     * @return denní čas simulace v minutách
     */
    public int getCurrentTime(){
        return currentTime;
    }

    /**
     * aktuální denní čas v minutách
     *
     * @return denní čas simulace v minutách
     */
    public int getCurrentTimeFromStart(){
        return currentTimeFromStart;
    }

    /**
     * Vrátí aktuální denní hodinu
     *
     * @return hodina
     */
    public int getCurrentHour(){
        return currentTime / 60;
    }

    /***
     * Vrátí instanci timeru
     * @return instance timeru
     */
    public static TimeTimer getTimer() {
        if (instance == null) {
            instance = new TimeTimer();
        }

        return instance;
    }

    /**
     * Vrátí v řetězci aktuální čas simulace včetně dne ve formátu Day: %d [%d:%d]
     *
     * @return aktuální den a čas
     */
    public String getCurrentTimeInStringForm(){
        int hour = currentTime / 60;
        int minute = currentTime % 60;
        //10:5
        if(hour >= 10 && minute >= 10) {
            return String.format("Day:%d [%d:%d]", currentDay, hour, minute);
        } else if(hour < 10 && minute >= 10) {
            return String.format("Day:%d [0%d:%d]", currentDay, hour, minute);
        } else if(hour >= 10 && minute < 10) {
            return String.format("Day:%d [%d:0%d]", currentDay, hour, minute);
        } else {
            return String.format("Day:%d [0%d:0%d]", currentDay, hour, minute);
        }
    }

    /**
     * Vrátí v řetězci aktuální čas simulace bez dne ve formátu %d:%d
     *
     * @return aktuální čas
     */
    public String getTimeString() {
        int hour = currentTime / 60;
        int minute = currentTime % 60;
        //10:5
        if(hour >= 10 && minute >= 10) {
            return String.format("%d:%d", hour, minute);
        } else if(hour < 10 && minute >= 10) {
            return String.format("0%d:%d", hour, minute);
        } else if(hour >= 10 && minute < 10) {
            return String.format("%d:0%d", hour, minute);
        } else {
            return String.format("0%d:0%d", hour, minute);
        }
    }



    /**
     * Posune o jednu minutu čas simulace dopředu. Případně aktualizuje den.
     */
    public void refreshTime() {
        if (currentTime < DAY_END) {
            currentTime += 1;
            currentTimeFromStart++;
        } else {
            currentTime = DAY_START;
            currentDay++;
        }
    }

    /**
     * Metoda vrací true pokud mohou pobočky ještě příjímat zboží nebo zadávat požadavky
     * false pokud ne
     *
     * @return true okno je otevřené, false okno je zavřené
     */
    public boolean isWindowOpen() {
        return currentTime >= REQUEST_START && currentTime < REQUEST_END;
    }

    /**
     * Metoda vrací true pokud je aktuálně celá
     *
     * @return true je celá hodina false není
     */
    public boolean isHour(){
        return currentTime % 60 == 0;
    }

    /**
     * Vrací v minutách kolik času zbývá do zavření okna
     *
     * @return počet minut do zavření okna
     */
    public int timeToWindowClose(){
        if(currentTime < REQUEST_END){
            return REQUEST_END - currentTime;
        }
        return 0;
    }

    /**
     * Nastaví výchozí hodnoty
     */
    private void start() {
        currentTime = 0;
        currentDay = 1;
    }
}
