package cz.zcu.kiv.nodes.abstracts;



import cz.zcu.kiv.data.generating.InstituteType;
import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.nodes.Bank;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.interfaces.IDrawable;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;


import java.util.Map;

public abstract class AFinancialInstitute implements IGraphNode, IDrawable {
    /***
     * nejvzdálenější souřadnice z kterou může mít instituce
     */
    public static final int MAX_X = 512;

    /***
     * nejvzdálenější souřadnice y kterou může mít instituce
     */
    public static final int MAX_Y = 512;

    /***
     * id instituce
     */
    protected int id;

    /***
     * jméno instituce
     */
    protected String name;

    /***
     * lokace instituce
     */
    protected Point point;

    /***
     * sousedi instituce
     */
    protected Map<Integer, Float> neighbors;

    /***
     * počet boxů v instituce
     */
    protected int packages;

    /***
     * Rozhoduje zda je institut vybraný v menu
     */
    protected boolean selected;

    /***
     * Vytvoří novou instanci finanční instituce
     *
     * @param id        id instituce
     * @param name      jméno instituce
     * @param point     lokace instituce
     * @param neighbors sousedi instituce
     */
    public AFinancialInstitute(int id, String name, Point point, Map<Integer, Float> neighbors) {
        this.id = id;
        this.name = name;
        this.point = point;
        this.neighbors = neighbors;
    }

    /***
     * Metoda zjistí o jaký typ instituce se jedná a ten typ vrátí
     *
     * @param aFinancialInstitute instituce neznámého typu
     * @return typ instituce
     */
    public static InstituteType determinateTypeOFInstitute(AFinancialInstitute aFinancialInstitute){
        if(aFinancialInstitute instanceof Bank){
            return InstituteType.BANK;
        }else if(aFinancialInstitute instanceof PartnerResidence){
            return InstituteType.PARTNER_INSTITUTE;
        }else if(aFinancialInstitute instanceof RegionalResidence){
            return InstituteType.REGIONAL_RESIDENCE;
        }else {
            return InstituteType.SKRBLIK;
        }
    }

    /***
     * Nastaví id instituce
     *
     * @param id id instituce
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Vrátí jméno instituce
     *
     * @return jméno instituce
     */
    public String getName() {
        return name;
    }

    /***
     * Nastaví jméno instituce
     *
     * @param name jméno instituce
     */
    public void setName(String name) {
        this.name = name;
    }

    /***
     * vrátí lokaci insitutce
     *
     * @return lokace instituce
     */
    public Point getPoint() {
        return point;
    }

    /***
     * Nastaví lokaci instituce
     *
     * @param point lokace instituce
     */
    public void setPoint(Point point) {
        this.point = point;
    }

    /***
     * Nastaví sousedy instituce
     *
     * @param neighbors sousedi instituce
     */
    public void setNeighbors(Map<Integer, Float> neighbors) {
        this.neighbors = neighbors;
    }

    /***
     * Nastaví označení instituce
     *
     * @param selected je označená true nebo false
     */
    public void setSelected(boolean selected){
        this.selected = selected;
    }

    /***
     * Vrátí id institutu
     *
     * @return id institu
     */
    @Override
    public int getId() {
        return this.id;
    }

    /***
     * Vrátí sousedy institudu
     *
     * @return sousedi institut
     */
    @Override
    public Map<Integer, Float> getNeighbors() {
        return this.neighbors;
    }

    /**
     * Vrátí počet balíčků s penězi
     * @return Počet balíčků s penězi
     */
    public int getPackages() {
        return packages;
    }

    public void setPackages(int packages) {
        this.packages = packages;
    }



    /***
     * Textový popis instance
     *
     * @return Textový popis instance
     */
    @Override
    public String toString() {
        return this.name;
    }
}
