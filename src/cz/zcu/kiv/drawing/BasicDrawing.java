
package cz.zcu.kiv.drawing;

import cz.zcu.kiv.Simulation;
import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.dispatcher.CarType;

import cz.zcu.kiv.drawing.custom.frames.CarFrame;
import cz.zcu.kiv.drawing.custom.frames.InstitutesFrame;
import cz.zcu.kiv.drawing.custom.frames.CompleteStatisticsTreeModel;
import cz.zcu.kiv.drawing.custom.frames.StatisticsFrame;
import cz.zcu.kiv.drawing.custom.frames.CarStatisticsTreeModel;

import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IDrawable;
import cz.zcu.kiv.statistics.StatisticsCounter;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.util.List;

public class BasicDrawing implements ILogOutput {
	/***
	 * Šířka pod oken
	 */
	public static final int ADDITIONAL_FRAME_WIDTH = 700;

	/***
	 * Výška pod oken
	 */
	public static final int ADDITIONAL_FRAME_HEIGHT = 200;

	/***
	 * Šířka oken se statistikou
	 */
	public static final int STATISTICS_FRAME_WIDTH = 1500;

	/***
	 * Výška oken se statistikou
	 */
	public static final int STATISTICS_FRAME_HEIGHT = 800;

	/***
	 * Základní šířka okna
	 */
	private static final int WIDTH = 1000;

	/***
	 * Základní výška okna
	 */
	private static final int HEIGHT = 750;

	/***
	 * Jediná instance třídy
	 */
	private static BasicDrawing instance = null;

	/***
	 * Okno aplikace
	 */
	private final JFrame frame;

	/***
	 * Panel s informacemi o městech
	 */
	private final DrawingPanel citiesPanel;

	/***
	 * Hlavní panel
	 */
	private JPanel mainPanel;

	/***
	 * Instance simulace
	 */
	private Simulation simulation;

	/***
	 * Města jako vykreslitelné prvky
	 */
	private List<IDrawable> drawables;

	/***
	 * Logovací textová oblast
	 */
	private JTextArea logTextArea;

	/***
	 * Startovací tlačítko simulace
	 */
	private JButton startButton;

	/***
	 * Tlačítko pro pozastavení a opětovné spuštění simulace
	 */
	private JButton pauseButton;

	/***
	 * Label pro čas
	 */
	private JLabel timeLabel;

	/***
	 * Frame obsahující kompletní statistiku bank
	 */
	private StatisticsFrame completeStatisticsFrame;

	/***
	 * Frame obsahující statistiku aut
	 */
	private StatisticsFrame carStatisticsFrame;

	/**
	 * [Pancerovany, Obrneny, Nakladní]
	 */
	private CarFrame[] carFrames = new CarFrame[3];

	/***
	 * Proměná určující zda je spuštěná simulace
	 */
	private boolean played = true;

	/***
	 * Vytvoří novou instanci třídy, kdy se vytváří všechny nezbytvné prvky
	 * Jframu a dalších pod framů
	 */
	private BasicDrawing() {
		this.citiesPanel = createCitiesPanel();
		frame = new JFrame();
		createCarFrames();
		createCompleteStatisticsFrames();
		createCarStatisticsFrame();
		setFrame();
	}

	/***
	 * Vrátí jedinou instanci jedináčka
	 *
	 * @return instance této třídy
	 */
	public static BasicDrawing getInstance(){
		if(instance == null){
			instance = new BasicDrawing();
		}

		return instance;
	}

	/***
	 * Přidá auto do vykreslovacího okna a do správného pod okna s informacemi
	 *
	 * @param car auto pro vykreslení
	 */
	public void addCarToDrawingList(Car car){
		CarType type = car.getType();
		citiesPanel.addCar(car);

		switch (type){
			case PANCEROVANY:
				carFrames[0].addCarToList(car);
				break;
			case OBRNENY:
				carFrames[1].addCarToList(car);
				break;
			case NAKLADNI:
				carFrames[2].addCarToList(car);
				break;
			default:
				Log.getLoger().log("Unknown car type", LogLevel.EMERGENCY);
		}
	}

	/***
	 * Nastaví města pod oknu s informacemi o městech
	 *
	 * @param cities města
	 */
	public void setCitiesList(AFinancialInstitute[] cities) {
		InstitutesFrame.getInstance().addDataToJList(cities);
	}

	/***
	 * Nastaví města která se budou vykreslovat
	 *
	 * @param drawables města pro vykreslení
	 */
	public void setNodes(List<IDrawable> drawables){
		if(frame != null && citiesPanel != null){
			this.drawables = drawables;
			this.citiesPanel.setGeneratedNodes(drawables);
			this.citiesPanel.repaint();
		}else {
			throw new NullPointerException("Není vytvořené herní okno!");
		}

		startButton.setEnabled(true);


	}

	/***
	 * Nastaví instanci simulace
	 *
	 * @param simulation instance simulace
	 */
	public void setSimulation(Simulation simulation){
		this.simulation = simulation;
		repaintAll();
	}

	/***
	 * Překreslí hlavní panel
	 */
	public void rapaintMainPanel(){
		mainPanel.repaint();
	}

	/***
	 * Překreslí všechny framy a některé jejich panely
	 */
	public void repaintAll() {
		mainPanel.repaint();
		if (InstitutesFrame.getInstance().isShowing()) {
			InstitutesFrame.getInstance().refreshInfo();
		}

		for(int i = 0; i  < carFrames.length; i++){
			if(carFrames[i].isShowing()){
				carFrames[i].refreshInfo();
			}
		}

	}

	/**
	 * Nastaví label představující čas simulace
	 *
	 * @param time čas simulace
	 */
	public void setTimeLabel(TimeTimer time){
		String sTime = time.getCurrentTimeInStringForm() + "  ";

		this.timeLabel.setText(sTime);
	}

	/***
	 * Nastaví parametry framu okna
	 */
	private void setFrame() {
		setWindow();

		frame.setTitle("Skrblik");
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/***
	 * Nastaví hlavní okno aplikace
	 */
	private void setWindow(){
		this.mainPanel 	= new JPanel();

		mainPanel.setLayout(new BorderLayout());

		mainPanel.add(createUpPanel(), BorderLayout.NORTH);
		mainPanel.add(citiesPanel ,BorderLayout.CENTER);
		mainPanel.add(createDownPanel(), BorderLayout.SOUTH);

		frame.add(mainPanel);
	}

	/***
	 * Vytvoří střed okna a vratí ho
	 *
	 * @return střed okna
	 */
	private DrawingPanel createCitiesPanel(){
		DrawingPanel citiesPanel = new DrawingPanel();
		citiesPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		return citiesPanel;
	}

	/***
	 * Vytvoří dolní panel okno s tlačítky a textovým polem
	 *
	 * @return dolní panel
	 */
	private JPanel createDownPanel(){
		JScrollPane scroll;

		JPanel downPanel = new JPanel(new BorderLayout());
		JPanel buttonsPanel = new JPanel();
		GridLayout gl = new GridLayout(1, 2);
		buttonsPanel.setLayout(gl);


		this.logTextArea = new JTextArea(10, 20);
		DefaultCaret caret = (DefaultCaret) logTextArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);


		this.startButton = new JButton("Start simulace");
		this.pauseButton = new JButton("Pause");

		scroll  = new JScrollPane(this.logTextArea);

		startButton.setEnabled(false);
		pauseButton.setEnabled(false);


		buttonsPanel.add(startButton);
		buttonsPanel.add(pauseButton);

		downPanel.add(scroll, BorderLayout.CENTER);
		downPanel.add(buttonsPanel, BorderLayout.SOUTH);

		startButton.addActionListener(event -> startSimulation());
		pauseButton.addActionListener(event -> pausePlaySimulation());

		return downPanel;
	}

	/***
	 * Vytvoří horní panel a vrátí ho
	 *
	 * @return horní panel
	 */
	private JMenuBar createUpPanel(){
		JMenuBar menuBar = new JMenuBar();

		JMenu menuInfo = new JMenu("Node Info");
		JMenu statistic = new JMenu("Statistic");
		JMenu carInfomenu = new JMenu("Cars Info");


		this.timeLabel = new JLabel("Day:1 [00:00]  ");

		JMenuItem hardArmoredItem = new JMenuItem("Hard Armored");
		JMenuItem armoredItem = new JMenuItem("Armored");
		JMenuItem lorryItem = new JMenuItem("Lorry");


		JMenuItem institutesInfoItem = new JMenuItem("Institutes Info");

		JMenuItem completeStatisticItem = new JMenuItem("Complete bank statistic");
		JMenuItem dayStatisticItem = new JMenuItem("Complete car statistic");

		carInfomenu.add(hardArmoredItem);
		carInfomenu.add(armoredItem);
		carInfomenu.add(lorryItem);

		menuInfo.add(carInfomenu);
		menuInfo.add(institutesInfoItem);

		statistic.add(completeStatisticItem);
		statistic.add(dayStatisticItem);


		menuBar.add(menuInfo);
		menuBar.add(statistic);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(timeLabel);

		hardArmoredItem.addActionListener(e -> openHardArmoredCarFrame());
		armoredItem.addActionListener(e -> openArmoredCarFrame());
		lorryItem.addActionListener(e -> openLorryFrame());

		completeStatisticItem.addActionListener(e -> openCompleteStatisticsFrame());
		dayStatisticItem.addActionListener(e -> openCarStatisticsFrame());

		institutesInfoItem.addActionListener(e -> openInstitutesInfoFrame());

		return menuBar;
	}

	/***
	 * Vytvoří oknna s informacemi o všech typech aut
	 */
	private void createCarFrames(){
		for(int i = 0; i < carFrames.length; i++){
			carFrames[i] = new CarFrame(ADDITIONAL_FRAME_WIDTH, ADDITIONAL_FRAME_HEIGHT);
		}
	}

	/***
	 * Vytvoří okno s kompletní statistikou bank
	 */
	private void createCompleteStatisticsFrames() {
		completeStatisticsFrame = new StatisticsFrame(STATISTICS_FRAME_WIDTH, STATISTICS_FRAME_HEIGHT, new CompleteStatisticsTreeModel());
		StatisticsCounter.getInstance().setCompleteStatisticsFrame(completeStatisticsFrame);
	}

	/***
	 * Vytvoří okno s kompletní statistikou aut
	 */
	private void createCarStatisticsFrame() {
		carStatisticsFrame = new StatisticsFrame(STATISTICS_FRAME_WIDTH, STATISTICS_FRAME_HEIGHT, new CarStatisticsTreeModel());
		StatisticsCounter.getInstance().passCarStatisticsFrame(carStatisticsFrame);
	}

	/***
	 * Otevře okno s informacemi o pancéřovaných autech
	 */
    private void openHardArmoredCarFrame() {
        carFrames[0].setVisible();
    }

	/**
	 * Otevře okno s informacemi o obrněných autech
	 */
	private void openArmoredCarFrame() {
		carFrames[1].setVisible();
	}

	/**
	 * Otevře okno s informacemi o náklaďácích
	 */
	private void openLorryFrame() {
		carFrames[2].setVisible();
	}

	/**
	 * Otevře okno s informacemi o institucích
	 */
    private void openInstitutesInfoFrame(){
		InstitutesFrame.getInstance().setVisible();
	}

	/***
	 * Spustí simulaci
	 */
	private void startSimulation(){
		if(drawables != null){
			simulation.startSimulation();
			pauseButton.setEnabled(true);
			startButton.setEnabled(false);
		}
	}

	/***
	 * Aktivuje tlačítko spustit simulaci a dekativuje pauzovací tlačítko
	 */
	public void reenableStartButton() {
		startButton.setEnabled(true);
		pauseButton.setEnabled(false);
	}

	/***
	 * Otevře okno s kompletní statistikou bank
	 */
	private void openCompleteStatisticsFrame() {
		completeStatisticsFrame.setVisible(true);
	}

	/***
	 * Otevře okno s kompletní statistikou aut
	 */
	private void openCarStatisticsFrame() {
		carStatisticsFrame.setVisible(true);
	}

	/***
	 * Pozastaví simulaci
	 */
	private void pausePlaySimulation(){

		if(played){
			simulation.pauseSimulation();
			pauseButton.setText("Play");
			played = false;
		}else {
			simulation.startSimulation();
			pauseButton.setText("Pause");
			played = true;
		}

	}

	/**
	 * Zobrazí zpráva do okna aplikace do příslušné textAreay
	 *
	 * @param message zpráva logu
	 */
	@Override
	public void showLogMessage(String message) {
		this.logTextArea.append(message + "\n");
	}
}
