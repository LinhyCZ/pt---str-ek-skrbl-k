package cz.zcu.kiv.statistics;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.zcu.kiv.Simulation;
import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.dispatcher.CarActions;
import cz.zcu.kiv.dispatcher.CarType;
import cz.zcu.kiv.dispatcher.MoneyRequest;
import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.drawing.custom.frames.CarEntry;
import cz.zcu.kiv.drawing.custom.frames.CarStatisticsTreeModel;
import cz.zcu.kiv.drawing.custom.frames.CompleteStatisticsTreeModel;
import cz.zcu.kiv.drawing.custom.frames.StatisticsFrame;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.abstracts.ABigBank;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;

import cz.zcu.kiv.nodes.interfaces.IMoneySender;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class StatisticsCounter {
    /***
     * Jediná instance
     */
    private static StatisticsCounter instance;

    /***
     * Instance loggeru
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Seznam statistik všech aut
     */
    private final List<CarStatistic> carStatistics = new ArrayList<>();

    /***
     * Instance timeru
     */
    private final TimeTimer timer = TimeTimer.getTimer();

    /***
     * Celkový počet požadavků
     */
    private int totalRequests = 0;

    /***
     * Celkový počet uspokojených požadavků
     */
    private int totalRequestsCompleted = 0;

    /***
     * Celkový počet požadavků za poslední tři dny
     */
    private int threeDayRequests = 0;

    /***
     * Celkový počet uspokojených požadavků za poslední tři dny
     */
    private int threeDayRequestsCompleted = 0;

    /***
     * Celkový počet požadavků za aktuální den
     */
    private int dailyRequests = 0;

    /***
     * Celkový počet uspokojených požadavků za aktuální den
     */
    private int dailyRequestsCompleted = 0;

    /***
     * Instance simulace
     */
    private Simulation simulation;

    /***
     * Frame obsahující statistiku bank
     */
    private StatisticsFrame completeStatisticsFrame;

    /***
     * Model pro vykreslení statistiky bank
     */
    private CompleteStatisticsTreeModel completeStatisticsmodel;

    /***
     * Frame obsahující statistiku aut
     */
    private StatisticsFrame carStatisticsFrame;

    /***
     * Model pro vykreslení statistiky aut
     */
    private CarStatisticsTreeModel carStatisticsModel;

    /***
     * Překladní tabulka pro smysluplné vypisování textu do výpisu
     */
    private final Map<Object, String> translationDictionary = new HashMap<>();

    /***
     * Konstruktor vyplní překladní tabulku
     */
    private StatisticsCounter() {
        translationDictionary.put(CarType.NAKLADNI, "nákladních");
        translationDictionary.put(CarType.PANCEROVANY, "pancéřovaných");
        translationDictionary.put(CarType.OBRNENY, "obrněných");
        translationDictionary.put(CarActions.LOADING, "nakládá");
        translationDictionary.put(CarActions.UNLOADING, "vykládá");
        translationDictionary.put(CarActions.WAITING, "čeká");
        translationDictionary.put(CarActions.PASSING, "je na cestě");
    }

    /***
     * Vrátí instanci počítadla
     * @return instance počítadla
     */
    public static StatisticsCounter getInstance() {
        if (instance == null) {
            instance = new StatisticsCounter();
        }

        return instance;
    }

    /***
     * Vyresetuje denní počítadlo
     */
    public void resetDaily() {
        this.dailyRequests = 0;
        this.dailyRequestsCompleted = 0;

        for (CarStatistic carStat : carStatistics) {
            carStat.resetDaily();
        }
    }

    /***
     * Vrátí počet denních žádostí
     * @return počet denních žádostí
     */
    public int getDailyRequests() {
        return this.dailyRequests;
    }

    /***
     * Vrátí počet uspokojených denních žádostí
     * @return počet uspokojených denních žádostí
     */
    public int getDailyRequestsCompleted() {
        return this.dailyRequestsCompleted;
    }

    /***
     * Vrátí celkový počet žádostí
     * @return celkový počet žádostí
     */
    public int getTotalRequests() {
        return this.totalRequests;
    }

    /***
     * Vrátí celkový počet uspokojených žádostí
     * @return celkový počet uspokojených žádostí
     */
    public int getTotalRequestsCompleted() {
        return this.totalRequestsCompleted;
    }

    /***
     * Zpracuje novou žádost o peníze
     */
    public void newRequest() {
        this.dailyRequests++;
        this.totalRequests++;
        this.threeDayRequests++;
    }

    /***
     * Zpracuje uspokojenou žádost o peníze
     * @param car auto
     * @param countOfRequest počet uspokojených žádostí
     * @param countOfPackages počet doručených balíčků
     * @param request uspokojená žádost
     */
    public void requestCompleted(Car car, int countOfRequest, int countOfPackages, MoneyRequest request) {
        this.totalRequestsCompleted += countOfRequest;
        this.dailyRequestsCompleted += countOfRequest;
        this.threeDayRequestsCompleted += countOfRequest;

        addCompletedRequestToStatisticsFrame(car, countOfRequest, countOfPackages, request);
    }

    /***
     * Zpracuje ukončení nakládání auta
     * @param car auto
     * @param countOfBoxes Počet naložených balíčků
     */
    public void loadingCompleted(Car car, int countOfBoxes) {
        carStatisticsModel.addRequestCompleted(car, countOfBoxes, (AFinancialInstitute) car.getOriginBank());
    }

    /***
     * Uloží novou statistiku auta
     * @param carStat nová statistika
     */
    public void newCarStatistic(CarStatistic carStat) {
        this.carStatistics.add(carStat);
        carStatisticsModel.addCar(carStat.getCar());
    }

    /***
     * Sečte denní žádosti u všech aut
     * @return součet denních žádostí
     */
    public int getAddedDailyRequests() {
        int value = 0;
        for (CarStatistic carStat : carStatistics) {
            value += carStat.getDailyRequests();
        }

        return value;
    }

    /***
     * Vypíše denní souhrn, vyresetuje denní počítadla, aktualizuje okna statistiky a každý třetí den zavolá funkci pro výpis třídenního souhrnu
     */
    public void newDay() {
        if (timer.getCurrentDay() != 1) {
            LOG.logSimulation("====================================================", LogLevel.INFO);
            LOG.logSimulation("|                                                  |", LogLevel.INFO);
            LOG.logSimulation("|                   Denní souhrn                   |", LogLevel.INFO);
            LOG.logSimulation("|                                                  |", LogLevel.INFO);
            LOG.logSimulation("====================================================", LogLevel.INFO);
            LOG.logSimulation("", LogLevel.INFO);
            LOG.logSimulation("Celkový počet nových požadavků za dnešek: " + this.getDailyRequests(), LogLevel.INFO);
            LOG.logSimulation("Celkový počet přiřazených požadavků za dnešek: " + this.getAddedDailyRequests(), LogLevel.INFO);
            LOG.logSimulation("Celkový počet uspokojených požadavků za dnešek: " + this.getDailyRequestsCompleted(), LogLevel.INFO);

            this.resetDaily();

            if ((timer.getCurrentDay() - 1) % 3 == 0) {
                showThreeDaySummary();
            }

        }

        addNewDayToWindow();
    }

    /***
     * Výpiše třídenní souhrn
     */
    public void showThreeDaySummary() {
        LOG.logSimulation("=======================================================", LogLevel.INFO);
        LOG.logSimulation("|                                                     |", LogLevel.INFO);
        LOG.logSimulation("|                   Třídenní souhrn                   |", LogLevel.INFO);
        LOG.logSimulation("|                                                     |", LogLevel.INFO);
        LOG.logSimulation("=======================================================", LogLevel.INFO);
        LOG.logSimulation("", LogLevel.INFO);
        LOG.logSimulation("Celkový počet požadavků za poslední tři dny: " + this.threeDayRequests, LogLevel.INFO);
        LOG.logSimulation("Celkový počet uspokojených požadavků za poslední tři dny: " + this.threeDayRequestsCompleted, LogLevel.INFO);

        this.threeDayRequests = 0;
        this.threeDayRequestsCompleted = 0;
    }

    /***
     * Zajistí výpis souhrnu na konci simulace a uložení dat statistiky do souboru
     */
    public void endOfSimulationSummary() {
        showTotalSummary();
        saveBankStatisticsGSON();
        saveCarStatisticsGSON();
    }

    /***
     * Vypíše celkový souhrn
     */
    public void showTotalSummary() {
        LOG.logSimulation("======================================================", LogLevel.INFO);
        LOG.logSimulation("|                                                    |", LogLevel.INFO);
        LOG.logSimulation("|                   Celkový souhrn                   |", LogLevel.INFO);
        LOG.logSimulation("|                                                    |", LogLevel.INFO);
        LOG.logSimulation("======================================================", LogLevel.INFO);
        LOG.logSimulation("", LogLevel.INFO);
        LOG.logSimulation("Celkový počet požadavků: " + this.getTotalRequests(), LogLevel.INFO);
        LOG.logSimulation("Celkový počet uspokojených požadavků: " + this.getTotalRequestsCompleted(), LogLevel.INFO);
        LOG.logSimulation("", LogLevel.INFO);
        showTotalCarSummary();
        LOG.logSimulation("", LogLevel.INFO);
    }

    /***
     * Uloží statistiku bank do souboru
     */
    public void saveBankStatisticsGSON() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(completeStatisticsmodel.getRoot());

        try {
            PrintWriter out = new PrintWriter("bankStatistic.json");
            out.println(json);
            out.close();
            LOG.logSimulation("Statistika o bankách byla uložena ve formátu JSON do souboru bankStatistic.json", LogLevel.INFO);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /***
     * Uloží statistiku aut do souboru
     */
    public void saveCarStatisticsGSON() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(carStatisticsModel.getRoot());

        try {
            PrintWriter out = new PrintWriter("carStatistic.json");
            out.println(json);
            out.close();
            LOG.logSimulation("Statistika o autech byla uložena ve formátu JSON do souboru carStatistic.json", LogLevel.INFO);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /***
     * Vypíše celkový souhrn vozidel
     */
    public void showTotalCarSummary() {
        CarEntry root = (CarEntry) carStatisticsModel.getRoot();

        for (CarEntry type : root.getDetails()) {
            LOG.logSimulation("Za simulaci převezlo " + type.getDetails().size() + " " + translationDictionary.get(type.getName()) + " aut celkem " + type.fullBoxesChange + " boxů s penězi", LogLevel.INFO);
        }
    }

    /***
     * Zajistí výpis hodinového souhrnu
     */
    public void showHourSummary() {
        Map<CarType, Map<CarActions, Integer>> hourCarStatistics = new HashMap<>();
        Map<CarType, Integer> hourCarBoxStatistics = new HashMap<>();

        showHourSummaryHeader();
        countHourSummaryData(hourCarStatistics, hourCarBoxStatistics);
        showHourSummaryMessage(hourCarStatistics, hourCarBoxStatistics);
    }

    /***
     * Vypíše tělo hodinového souhrnu
     * @param hourCarStatistics hodinové statistiky aut
     * @param hourCarBoxStatistics hodinové statistiky balíčků
     */
    private void showHourSummaryMessage(Map<CarType, Map<CarActions, Integer>> hourCarStatistics, Map<CarType, Integer> hourCarBoxStatistics) {
        CarActions[] countActions = new CarActions[] {CarActions.LOADING, CarActions.UNLOADING, CarActions.WAITING, CarActions.PASSING};

        Set<CarType> types = hourCarStatistics.keySet();
        for (CarType keyType : types) {
            LOG.logSimulation("", LogLevel.INFO);
            LOG.logSimulation("Aktuálně je na cestě "
                    + (hourCarStatistics.get(keyType).get(CarActions.ALL) == null ? 0 : hourCarStatistics.get(keyType).get(CarActions.ALL)) + " "
                    + translationDictionary.get(keyType) + " aut, která převážejí dohromady "
                    + (hourCarBoxStatistics.get(keyType) == null ? 0 : hourCarBoxStatistics.get(keyType))
                    + " boxů s penězi", LogLevel.INFO);

            LOG.logSimulation("Z toho:", LogLevel.INFO);
            for (CarActions keyAction : countActions) {
                LOG.logSimulation("- " + (hourCarStatistics.get(keyType).get(keyAction) == null ? 0 : hourCarStatistics.get(keyType).get(keyAction))
                        + " aut " + translationDictionary.get(keyAction), LogLevel.INFO);
            }
        }
    }

    /***
     * Upraví akce na čtyři základní akce důležité pro výpis
     * @param action výchozí akce
     * @return upravená akce
     */
    private CarActions changeAction(CarActions action) {
        CarActions tempAction = action;

        switch (tempAction) {
            case LOADING_DONE:
                tempAction = CarActions.LOADING;
                break;
            case UNLOADING_DONE:
                tempAction = CarActions.UNLOADING;
                break;
            case WAITING_LOADING:
            case WAITING_UNLOADING:
                tempAction = CarActions.WAITING;
                break;
            default:
                LOG.logSimulation("Wat", LogLevel.DEBUG);
        }

        return tempAction;
    }

    /***
     * Sečte data pro hodinový souhrn
     * @param hourCarStatistics hodinové statistiky aut
     * @param hourCarBoxStatistics hodinové statistiky balíčků
     */
    private void countHourSummaryData(Map<CarType, Map<CarActions, Integer>> hourCarStatistics, Map<CarType, Integer> hourCarBoxStatistics) {
        for (CarType val : CarType.values()) {
            hourCarStatistics.put(val, new HashMap<>());
        }

        Map<IMoneySender, List<Car>> map = Dispatcher.getDispatcher().getCarList();
        Set<IMoneySender> set = map.keySet();
        for (IMoneySender key : set) {
            List<Car> list = map.get(key);
            for (Car car : list) {
                CarActions tempAction = changeAction(car.getAction());

                hourCarBoxStatistics.put(car.getType(),
                         (hourCarBoxStatistics.get(car.getType()) == null ? car.getTotalBoxes() : hourCarBoxStatistics.get(car.getType()) + car.getTotalBoxes()));

                hourCarStatistics.get(car.getType()).put(tempAction,
                         (hourCarStatistics.get(car.getType()).get(tempAction) == null ? 1 : hourCarStatistics.get(car.getType()).get(tempAction) + 1)); //Add 1 to counter with given action and type

                 hourCarStatistics.get(car.getType()).put(CarActions.ALL,
                         (hourCarStatistics.get(car.getType()).get(CarActions.ALL) == null ? 1 : hourCarStatistics.get(car.getType()).get(CarActions.ALL) + 1)); //Add 1 to counter of all cars
            }
        }
    }

    /***
     * Vypíše hlavičku hodinového souhrnu
     */
    private void showHourSummaryHeader() {
        LOG.logSimulation("=======================================================", LogLevel.INFO);
        LOG.logSimulation("|                                                     |", LogLevel.INFO);
        LOG.logSimulation("|                   Hodinový souhrn                   |", LogLevel.INFO);
        LOG.logSimulation("|                                                     |", LogLevel.INFO);
        LOG.logSimulation("=======================================================", LogLevel.INFO);
        LOG.logSimulation("", LogLevel.INFO);
        LOG.logSimulation("Celkový počet žádostí: " + this.getDailyRequests(), LogLevel.INFO);
        LOG.logSimulation("", LogLevel.INFO);
        LOG.logSimulation("Stav bank:", LogLevel.INFO);
        printStatesOfBanks();

        updateStatsWindow();
    }

    /***
     * Vypíše počet boxů ve všech bankách
     */
    public void printStatesOfBanks(){
        for(ABigBank bank : simulation.getRegialResidencesAndBank()){
            LOG.logSimulation(String.format("Stav banky %d boxů - %s ", bank.getPackages(), bank.getName()), LogLevel.INFO);
        }
    }

    /***
     * Nastaví simulace
     * @param simulation simulace
     */
    public void setSimulation(Simulation simulation) {
        this.simulation = simulation;
    }

    /***
     * Předá frame s kompletní statistikou bank
     * @param frame frame s kompletní statistikou bank
     */
    public void setCompleteStatisticsFrame(StatisticsFrame frame) {
        this.completeStatisticsFrame = frame;
    }

    /***
     * Předá frame s kompletní statistikou aut
     * @param frame frame s kompletní statistikou aut
     */
    public void passCarStatisticsFrame(StatisticsFrame frame) {
        this.carStatisticsFrame = frame;
    }

    /***
     * Provede výchozí nastavení framu kompletní statistiky bank
     */
    public void initializeCompleteStatisticsFrame() {
        completeStatisticsmodel = (CompleteStatisticsTreeModel) this.completeStatisticsFrame.getModel();
        for (AFinancialInstitute institute : Dispatcher.aFinancialInstitutes) {
            completeStatisticsmodel.addBank(institute);
        }

        updateStatsWindow();
    }

    /***
     * Přidá nový den do okna statistiky bank
     */
    public void addNewDayToWindow() {
        int day = timer.getCurrentDay();
        for (AFinancialInstitute institute : Dispatcher.aFinancialInstitutes) {
            completeStatisticsmodel.addDay(institute, day);
        }
    }

    /***
     * Přidá dokončenou žádost o peníze do statistik
     * @param car auto
     * @param countOfRequest počet uspokojených žádostí
     * @param countOfPackages počet doručených balíčků
     * @param request žádost o peníze
     */
    public void addCompletedRequestToStatisticsFrame(Car car, int countOfRequest, int countOfPackages, MoneyRequest request) {
        completeStatisticsmodel.addCar(request.getOriginNode(), timer.getCurrentDay(), timer.getTimeString(), countOfRequest, countOfPackages, car);
        carStatisticsModel.addRequestCompleted(car, countOfPackages * -1, request.getOriginNode());
    }

    /***
     * Provede výchozí nastavení framu kompletní statistiky aut
     */
    public void initializeCarStatisticsFrame() {
        carStatisticsModel = (CarStatisticsTreeModel) this.carStatisticsFrame.getModel();
        for (CarType type : CarType.values()) {
            carStatisticsModel.addCarType(type);
        }

        updateStatsWindow();
    }

    /***
     * Aktualizuje obě okna se statistikou
     */
    public void updateStatsWindow() {
        this.completeStatisticsFrame.updateTree();
        this.carStatisticsFrame.updateTree();
    }
}
