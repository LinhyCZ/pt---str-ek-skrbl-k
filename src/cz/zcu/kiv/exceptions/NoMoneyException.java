package cz.zcu.kiv.exceptions;

/***
 * Exception pokud auto žadá o peníze, ale v bance už peníze nejsou
 */
public class NoMoneyException extends Exception {
    public NoMoneyException(String errorMessage) {
        super(errorMessage);
    }
}
