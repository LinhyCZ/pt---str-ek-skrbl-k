package cz.zcu.kiv.drawing.custom.frames;

import com.google.gson.annotations.Expose;

import java.util.LinkedList;
import java.util.List;

public class CarEntry {
    /***
     * Jméno prvku
     */
    @Expose
    private final Object name;

    /***
     * Cesta auta
     */
    @Expose
    protected String path;

    /***
     * Změna boxů v autě
     */
    @Expose
    public int fullBoxesChange;

    /***
     * Změna prázdných boxů v autě
     */
    @Expose
    protected int emptyBoxesChange;

    /***
     * Seznam dalších detailů
     */
    @Expose
    protected final List<CarEntry> details;

    /***
     * Cesta auta, ale místo čárek jako oddělovačů jsou použity šipky =&gt;
     */
    protected String pathWithArrows;

    /***
     * Prvek je poslední
     */
    private final boolean isLeaf;

    /***
     * Konstruktor prvku
     * @param name Jméno prvku
     * @param path Cesta auta
     * @param fullBoxesChange Změna boxů v autě
     * @param emptyBoxesChange Změna prázdných boxů v autě
     * @param isLeaf Prvek je poslední
     */
    public CarEntry(Object name, String path, int fullBoxesChange, int emptyBoxesChange, boolean isLeaf) {
        this.name = name;
        this.isLeaf = isLeaf;

        if (!isLeaf) {
            details = new LinkedList<>();
        } else {
            details = null;
        }

        this.pathWithArrows = path;
        this.path = (path == null ? null : path.replaceAll("=>", ","));
        this.fullBoxesChange = fullBoxesChange;
        this.emptyBoxesChange = emptyBoxesChange;

    }

    /***
     * Vrátí detaily prvku
     * @return detaily prvku
     */
    public List<CarEntry> getDetails() {
        return details;
    }

    /***
     * Vrátí jestli je prvek koncový
     * @return true = prvek je koncový, false = prvek není koncový
     */
    public boolean isLeaf() {
        return isLeaf;
    }

    /***
     * Vrátí jméno prvku
     * @return jméno prvku
     */
    public Object getName() {
        return name;
    }

    /***
     * Vrátí jméno prvku
     * @return jméno prvku
     */
    public String toString() {
        return name.toString();
    }
}
