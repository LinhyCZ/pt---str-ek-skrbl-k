package cz.zcu.kiv.nodes.abstracts;

import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.nodes.interfaces.IMoneySender;

import java.util.Map;

public abstract class ABigBank extends AFinancialInstitute implements IMoneySender {


    /***
     * Vytvoří novou instanci velké banky
     *
     * @param id        id instituce
     * @param name      jméno instituce
     * @param point     lokace instituce
     * @param neighbors sousedi instituce
     */
    public ABigBank(int id, String name, Point point, Map<Integer, Float> neighbors) {
        super(id, name, point, neighbors);
    }

    /**
     * Odečte počet balíčků s penězi z konta. Pokud na kontě není dostatek peněz, vrátí maximální počet peněz a ty odečte z konta (Zabrání zápornému stavu konta)
     * @param packages Požadované balíčky
     * @return Poskytnuté balíčky
     */
    @Override
    public int subtractPackages(int packages) {
        if (this.packages < packages) {
            int toReturn = this.packages;
            this.packages = 0;
            return toReturn;
        }

        this.packages -= packages;
        return packages;
    }

    /**
     * Přidá zadaný počet balíčků s penězi na dané konto
     * @param packages Počet balíčků s penězi
     */
    @Override
    public void addPackages(int packages) {
        this.packages += packages;
    }

    /***
     * Na základě vloženého parametru vrací odpověď jestli má nebo nemá (boxy) peníze
     *
     * @param numberOfRequestedBoxes počet požadovaných boxů
     * @return true má volné boxy false nemá volné boxy
     */
    @Override
    public boolean hasMoney(int numberOfRequestedBoxes) {
        return this.packages > numberOfRequestedBoxes;
    }
}
