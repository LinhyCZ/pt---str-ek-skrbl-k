package cz.zcu.kiv.drawing.custom.frames;

import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.dispatcher.CarType;
import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;
import de.javagl.treetable.AbstractTreeTableModel;
import de.javagl.treetable.TreeTableModel;

import java.util.HashMap;
import java.util.Map;

public class CarStatisticsTreeModel extends AbstractTreeTableModel {
    /***
     * Jména sloupců
     */
    static protected String[] cNames = {"", "Cesta", "Počet boxů", "Počet prázdných boxů"};

    /***
     * Typy sloupců
     */
    static protected Class[]  cTypes = { TreeTableModel.class, String.class, String.class, String.class };

    /***
     * Indexy ID aut v seznamu
     */
    private final Map<Integer, Integer> carIndexes = new HashMap<>();

    /***
     * Konstruktor modelu
     */
    public CarStatisticsTreeModel() {
        super(new CarEntry("Auta", null, 0, 0, false));
        //rootEntry = (Entry) super.getRoot();
    }

    /***
     * Vrátí počet potomků daného prvku
     * @param node daný prvek
     * @return počet potomků
     */
    public int getChildCount(Object node) {
        if (!((CarEntry) node).isLeaf()) {
            return ((CarEntry) node).getDetails().size();
        }
        return 0;
    }

    /***
     * Vrátí potomka na zadné pozici
     * @param node prvek
     * @param i pozice potomka
     * @return potomek
     */
    public Object getChild(Object node, int i) {
        return ((CarEntry) node).getDetails().get(i);
    }

    /***
     * Vrátí jestli je daný prvek koncový
     * @param node zadaný prvek
     * @return true = prvek je koncový, false = prvek není koncový
     */
    public boolean isLeaf(Object node) {
        return ((CarEntry) node).isLeaf();
    }

    /***
     * Vrátí počet sloupců
     * @return počet sloupců
     */
    public int getColumnCount() {
        return cNames.length;
    }

    /***
     * Vrátí název daného sloupce
     * @param column pořadí sloupce
     * @return název daného sloupce
     */
    public String getColumnName(int column) {
        return cNames[column];
    }

    /***
     * Vrátí třídu daného sloupce
     * @param column pořadí sloupce
     * @return třída daného sloupce
     */
    public Class getColumnClass(int column) {
        return cTypes[column];
    }

    /***
     * Vrátí hodnotu daného sloupce pro daný prvek
     * @param node prvek
     * @param column pořadí sloupce
     * @return hodnota daného sloupce pro daný prvek
     */
    public Object getValueAt(Object node, int column) {
        switch(column) {
            case 0:
                return node;
            case 1:
                return (((CarEntry)node).pathWithArrows == null ? "" : ((CarEntry)node).pathWithArrows);
            case 2:
                if (node != getRoot()) {
                    return ((CarEntry)node).fullBoxesChange;
                }

                return "";
            case 3:
                if (node != getRoot()) {
                    return ((CarEntry)node).emptyBoxesChange;
                }

                return "";
            default:
                System.out.println("Unknown column");
        }

        return null;
    }

    /***
     * Přidá typ vozidla do stromu
     * @param type typ vozidla
     */
    public void addCarType(CarType type) {
        ((CarEntry) getRoot()).details.add(new CarEntry(type, null,0, 0, false));
    }

    /***
     * Přidá auto do stromu
     * @param car vozidlo
     */
    public void addCar(Car car) {
        CarEntry typeEntry = ((CarEntry) getRoot()).details.get(car.getType().ordinal());

        CarEntry newEntry = new CarEntry("Auto ID: " + car.getID(), null, 0, 0, false);
        carIndexes.put(car.getID(), typeEntry.details.size());
        typeEntry.details.add(newEntry);
    }

    /***
     * Přidá uspokojený požadavek na peníze do stromu
     * @param car auto
     * @param packages počet balíčků
     * @param origin místo, kde požadavek vznikl
     */
    public void addRequestCompleted(Car car, int packages, AFinancialInstitute origin) {
        String pathValue = "";
        if (car.getStats().getCurrentPath() != null) {
            for (IGraphNode node : car.getStats().getCurrentPath()) {
                pathValue += Dispatcher.aFinancialInstitutes[node.getId()].getName() + " => ";
            }

            pathValue = pathValue.substring(0, pathValue.length() - 3);
        }

        car.getStats().setCurrentPath(null);

        CarEntry typeEntry = ((CarEntry) getRoot()).details.get(car.getType().ordinal());
        if (packages < 0) {
            typeEntry.fullBoxesChange += packages * -1;
            typeEntry.emptyBoxesChange += packages * -1;
        }

        CarEntry carEntry = typeEntry.details.get(carIndexes.get(car.getID()));
        if (packages < 0) {
            carEntry.fullBoxesChange += packages * -1;
            carEntry.emptyBoxesChange += packages * -1;
        }

        CarEntry newEntry;
        if (carEntry.details.size() == 0) {
            newEntry = new CarEntry(origin.getName(), pathValue, packages, 0, true);
        } else {
            newEntry = new CarEntry(origin.getName(), pathValue, packages, packages * -1, true);
        }

        carEntry.details.add(newEntry);
    }
}
