package cz.zcu.kiv.dispatcher;

import cz.zcu.kiv.data.generating.InstituteType;
import cz.zcu.kiv.drawing.BasicDrawing;
import cz.zcu.kiv.exceptions.NoMoneyException;
import cz.zcu.kiv.graphing.FloydWarshall;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.Bank;
import cz.zcu.kiv.nodes.PartnerResidence;
import cz.zcu.kiv.nodes.RegionalResidence;
import cz.zcu.kiv.nodes.SkrblikResidence;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IMoneySender;
import cz.zcu.kiv.statistics.StatisticsCounter;
import javafx.util.Pair;

import java.util.*;

public class Dispatcher {
    /***
     * Časová tolerance při vyhledávání nejbližší žádosti
     */
    private final int TIME_TOLERANCE = 15;

    /***
     * Instance loggeru
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Seznam finančních institutů
     */
    public static AFinancialInstitute[] aFinancialInstitutes;

    /***
     * Instnace jedináčka
     */
    private static Dispatcher instance = null;

    /***
     * Seznam požadavků u dané banky (Například jedna banka má více žádostí o peníze)
     */
    private final Map<AFinancialInstitute, ArrayList<MoneyRequest>> institutesWithRequests = new HashMap<>();

    /***
     * Seznam vozidel dané banky
     */
    private final Map<IMoneySender, List<Car>> carList = new HashMap<>();

    /***
     * Seznam naložených čekajících vozidel
     */
    private final Map<IMoneySender, List<Car>> loadedCarsList = new HashMap<>();

    /***
     * Instance FloyfWarshalla
     */
    private FloydWarshall fw;

    /***
     * List finančních institutů
     */
    private List<AFinancialInstitute> AFinancialInstitutes;

    /***
     * Privátní konstruktor
     */
    private Dispatcher(){

    }

    /***
     * Vrátí instanci jedináčka Dispatchera
     * @return instance Dispatchera
     */
    public static Dispatcher getDispatcher(){
        if(instance == null){
            instance = new Dispatcher();
        }
        return instance;
    }

    /***
     * Přiřadí dispatcherovi instanci FloydWarshalla
     * @param fw instance FloydWarshalla
     */
    public void setFloydWarshall(FloydWarshall fw){
        this.fw = fw;
    }

    /***
     * Přiřadí dispatcherovi seznam finančních institutů
     * @param AFinancialInstitutes seznam finančních institutů
     */
    public void setAFinancialInstitutes(AFinancialInstitute[] AFinancialInstitutes){
        this.AFinancialInstitutes = Arrays.asList(AFinancialInstitutes);
    }

    /***
     * Metoda vrací počet požadavků kolik kterých má instituce
     *
     * @param institute instituce u které chceme znát počet požadavků na boxy
     * @return  počet požadavků (boxů) které instituce chce
     */
    public int getNumberOfBankRequest(AFinancialInstitute institute){
        int numberOfBoxes = 0;

        if(institutesWithRequests.get(institute) != null){
            numberOfBoxes = institutesWithRequests.get(institute).stream().mapToInt(MoneyRequest::getNumberOfBoxes).sum();
        }

        return numberOfBoxes;
    }

    /***
     * Přidá žádost do seznamu požadavků
     *
     * @param request žádost o peníze
     * @return true = request se přiřadil; false = request se nepřiřadil
     */
    public boolean newRequest(MoneyRequest request) {
        AFinancialInstitute requestedFrom = request.getOriginNode();
        StatisticsCounter.getInstance().newRequest();
        if (!institutesWithRequests.containsKey(requestedFrom)) {
            institutesWithRequests.put(requestedFrom, new ArrayList<>());
        }

        this.institutesWithRequests.get(requestedFrom).add(request);

        return assignRequestToCar(request);
    }

    /***
     * Z dané žádosti najde nejbližší institut, u kterého může podat žádost o peníze
     * @param request žádost o peníze
     * @return nejbližší institut
     */
    public IMoneySender findNearestInstitute(MoneyRequest request) {
        InstituteType required = request.getRequiredInstitute();
        AFinancialInstitute origin = request.getOriginNode();

        List<AFinancialInstitute> list = this.AFinancialInstitutes;
        Iterator<AFinancialInstitute> listIterator = list.iterator();
        float minDistance = Float.MAX_VALUE;
        IMoneySender minNode = null;

        while (listIterator.hasNext()) {
            AFinancialInstitute toCompare = listIterator.next();
            InstituteType typeToCompare = AFinancialInstitute.determinateTypeOFInstitute(toCompare);
            float lenghtBetweenCities = fw.pathLengthBetweenCities(toCompare.getId(), origin.getId());
            if (typeToCompare == required && lenghtBetweenCities < minDistance) {
                minDistance = lenghtBetweenCities;
                minNode = (IMoneySender) toCompare;
            }
        }

        return minNode;
    }

    /***
     * Přiřadí nejlepšímu vozidlu danou žádost. Pokud už do banky jede auto uspokojit žádost, bude toto auto preferováno, pokud má dostatek balíčků. Pokud k bance jede více automobilů,
     * bude preferováno vozidlo, které je blíže. Pokud k bance žádné vozidlo nejede, bude vybráno jiné nejvhodnější vozidlo, případně vytvořeno nové.
     *
     * @param request žádost, kerou je nutné uspokojit
     * @return true = auto se podařilo přiřadit; false = auto se nepodařilo přiřadit
     */
    public boolean assignRequestToCar(MoneyRequest request) {
        Car car = null;
        if (institutesWithRequests.get(request.getOriginNode()) != null) {
            car = assignRequestToCarIterator(request);
        }

        if (car == null) {
            try {
                car = findNearestAvailableCar(request.getOriginNode(), (AFinancialInstitute)this.findNearestInstitute(request), request.getNumberOfBoxes());
            } catch (NoMoneyException e) {
                Log.getLoger().log(e.toString(), LogLevel.CRITICAL);
                return false;
            }

        }

        if (car == null || !car.acceptRequest(request)) {
            LOG.logSimulation("Nepodařilo se přiřadit žádost k danému vozidlu! Žádost bude neuspokojena! Žádost: " + request.getNumberOfBoxes() + ", Žádost do: " + request.getOriginNode(), LogLevel.CRITICAL);
            return false;
        }

        return true;
    }

    /***
     * Prohledá seznam aut u banky s žádostmi a vrátí auto, které jede nejrychleji k bance
     * @param request Žádost o peníze
     * @return Nejbližší auto
     */
    private Car assignRequestToCarIterator(MoneyRequest request) {
        double minTime = Double.MAX_VALUE;
        Car car = null;
        Iterator<MoneyRequest> iterator = institutesWithRequests.get(request.getOriginNode()).iterator();
        while (iterator.hasNext()) {
            Car tempCar = iterator.next().getCar();
            if (tempCar != null
                    && ((request.getOriginNode() instanceof PartnerResidence && tempCar.getType() == CarType.NAKLADNI)
                    || (request.getOriginNode() instanceof SkrblikResidence && tempCar.getType() == CarType.OBRNENY)
                    || (request.getOriginNode() instanceof RegionalResidence && tempCar.getType() == CarType.PANCEROVANY))) {
                    Pair<Car, Double> pair = checkCar(car, tempCar, request, minTime);
                    minTime = pair.getValue();
                    car = pair.getKey();
            }
        }

        return car;
    }

    /***
     * Druhá část kontroly z iterátoru
     * @param car Staré auto
     * @param tempCar dočasné auto, které prošlo první částí podmínek
     * @param request Žádost o peníze
     * @param minTime aktuální minimální čas
     * @return Pár klíče (Aktuálně nejvýhodnější auto) a minimálního času
     */
    private Pair<Car, Double> checkCar(Car car, Car tempCar, MoneyRequest request, double minTime) {
        if (CarUtils.getTimeToArrive(tempCar, request.getOriginNode()) < minTime && tempCar.getMoneyAvailable() >= request.getNumberOfBoxes() && !tempCar.isHeadingToLoading()) {
            double newMinTime = CarUtils.getTimeToArrive(tempCar, request.getOriginNode());
            return new Pair<>(tempCar, newMinTime);
        } else {
            return new Pair<>(car, minTime);
        }
    }


    /***
     * Přidá auto do seznamu aut čekajících v bance na další cestu
     * @param bank Banka ve které auto čeká
     * @param car Auto
     */
    public void addCarToList(IMoneySender bank, Car car) {
        if (this.carList.get(bank) == null) {
            this.carList.put(bank, new ArrayList<>());
        }
        this.carList.get(bank).add(car);
        BasicDrawing.getInstance().addCarToDrawingList(car);
    }

    /***
     * Najde nejlbižší dostupné auto dané destinaci
     *
     * @param destination Destinace, kam má auto jet
     * @param origin Banka, ze které auto pochází
     * @param requiredPackages Počet požadovaných boxů
     * @return Dané auto
     * @throws NoMoneyException Nejsou peníze
     */
    public Car findNearestAvailableCar(AFinancialInstitute destination, AFinancialInstitute origin, int requiredPackages) throws NoMoneyException {
        int minTime = Integer.MAX_VALUE;
        Car nearestCar = null;

        if (this.carList.get(origin) != null) {
            Iterator<Car> iterator = this.carList.get(origin).iterator();
            while (iterator.hasNext()) {
                Car car = iterator.next();
                int time = CarUtils.getTimeToArrive(car, destination);
                if (time < minTime && car.getMoneyAvailable() >= requiredPackages) {
                    minTime = time;
                    nearestCar = car;
                }
            }
        }

        minTime -= TIME_TOLERANCE;

        if(minTime > checkTime(destination, origin) || nearestCar == null) {
            nearestCar = getNearestCar(origin, destination, requiredPackages, nearestCar);

            if (nearestCar == null) {
                /* TODO Fronta požadavků u banky - jakmile jsou peníze na požadavek, naložit auto a vyrazit s ním na cestu */
                throw new NoMoneyException("Banka (ID: " + origin.getId() + ") nemá dostatek peněz na uspokojení požadavku! Požadované peníze: " + requiredPackages + ", Dostupné peníze: " + origin.getPackages());
            }
        }

        return nearestCar;
    }

    /***
     * Vrátí nejbližší auto ze seznamu naložených aut
     * @param origin Výchozí banka
     * @param destination Cílová banka
     * @param requiredPackages požadovaný počet boxů
     * @param newNearestCar Prozatimní nejbližší auto
     * @return Nové nejbližší auto
     */
    public Car getNearestCar(AFinancialInstitute origin, AFinancialInstitute destination, int requiredPackages, Car newNearestCar) {
        Car nearestCar = newNearestCar;
        if (loadedCarsList.get(origin) != null && !loadedCarsList.get(origin).isEmpty()) {
            for (Car carLoop : loadedCarsList.get(origin)) {
                if (carLoop.getMoneyAvailable() >= requiredPackages) {
                    nearestCar = carLoop;
                    loadedCarsList.get(origin).remove(nearestCar);
                    break;
                } else if (validateNewCar(origin, requiredPackages)) {
                    nearestCar = loadedCarsList.get(origin).get(0);
                    nearestCar.loadingMoney();
                    loadedCarsList.get(origin).remove(nearestCar);
                    break;
                }
            }
        } else if (validateNewCar(origin, requiredPackages)) {
            nearestCar = createNewNearestCar(origin, destination);
        }

        return nearestCar;
    }

    /***
     * Určí typ nového auta a vytvoří ho
     * @param origin výchozí město
     * @param destination cílová destinace
     * @return Instance auta
     */
    private Car createNewNearestCar(AFinancialInstitute origin, AFinancialInstitute destination) {
        CarType type;
        //kontrola zda jede auto z banky do skrblíkovi pobočky
        if (origin instanceof Bank && destination instanceof SkrblikResidence) {
            type = CarType.OBRNENY;
        } else if (origin instanceof Bank && destination instanceof RegionalResidence) {
            type = CarType.PANCEROVANY;
        } else {
            type = CarType.NAKLADNI;
        }

        Log.getLoger().log("V bance (ID: " + origin.getId() + ") zbývá boxů: " + origin.getPackages(), LogLevel.DEBUG);
        return createNewCar(origin, type);
    }

    /***
     * Vytvoří nové auto a naloží ho penězmi
     * @param origin výchozí město
     * @param type Typ auta
     * @return Instance auta
     */
    public Car createNewCar(AFinancialInstitute origin, CarType type) {
        Car newCar = new Car(type, (IMoneySender) origin, fw);
        newCar.loadingMoney();
        this.addCarToList((IMoneySender)origin, newCar);
        return newCar;
    }

    /***
     * Ověří jestli je dostatek peněz ve specifikované bance
     * @param origin banka, ve které se ověřuje
     * @param requiredPackages požadované peníze
     * @return true = dostatek peněz, false = nedostatek peněz
     */
    public boolean validateNewCar(AFinancialInstitute origin, int requiredPackages) {
        return origin.getPackages() >= requiredPackages;
    }

    /***
     * Vypočítá čas auta mezi danými bankami
     * @param destination výchozí banka
     * @param origin cílová banka
     * @return čas potřebný pro jízdu v minutách
     */
    private int checkTime(AFinancialInstitute destination, AFinancialInstitute origin) {
        CarType type;
        //kontrola zda jde o pobočku skrblíka
        if (origin instanceof Bank && destination instanceof SkrblikResidence) {
            type = CarType.OBRNENY;
        } else if (origin instanceof Bank && destination instanceof RegionalResidence) {
            type = CarType.PANCEROVANY;
        } else {
            type = CarType.NAKLADNI;
        }

        int time = CarUtils.calculateDistanceTime(fw.pathLengthBetweenCities(destination.getId(), origin.getId()), type);
        //time += (Math.min(type.getCapacity(), origin.getPackages()) * 5); Edit -> Nakladani a vykladani balicku lze zanedbat -> Viz CW->PT->Majdis->Akutality

        return time;
    }

    /***
     * Pro každé auto v seznamu zavolá funkci clockEvent()
     * @param time aktuální čas simulace v minutách
     */
    public void refreshCars(int time) {
        for (IMoneySender sender : carList.keySet()) {
            List<Car> cars = this.carList.get(sender);

            if (cars != null) {
                for (Car car : cars) {
                    car.clockEvent(time);
                }
            }
        }
    }

    /***
     * Uloží auto na seznam aut dostupných na cestu
     * @param car Auto
     */
    public void addLoadedCar(Car car) {
        loadedCarsList.computeIfAbsent(car.getOriginBank(), k -> new LinkedList<>());

        loadedCarsList.get(car.getOriginBank()).add(car);
    }

    /***
     * Odstraní auto ze seznamu aut dostupných na cestu
     * @param car Auto
     */
    public void removeLoadedCar(Car car) {
        if (loadedCarsList.get(car.getOriginBank()) != null) {
            loadedCarsList.get(car.getOriginBank()).remove(car);
        }
    }

    /***
     * Vrátí seznam aut
     * @return seznam aut
     */
    public Map<IMoneySender, List<Car>> getCarList() {
        return this.carList;
    }
}
