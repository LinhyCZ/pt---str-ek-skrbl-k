package cz.zcu.kiv.drawing.custom.frames;

import cz.zcu.kiv.dispatcher.Car;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public  class CarFrame extends JFrame {

    /***
     * Levý panel+
     */
    private final JPanel left;

    /***
     * Prostřední pravý panel
     */
    private final JPanel center;

    /***
     * Id auta
     */
    private JTextField idTF;

    /***
     * Akce kterou auto provádí
     */
    private JTextField actionTF;

    /***
     * Aktuální město kde auto je (vyjelo)
     */
    private JTextField originCityTF;

    /***
     * Aktuální město kde auto je (vyjelo)
     */
    private JTextField currentCityTF;

    /***
     * Toxtové pole s informací o dalším městě přes které auto bude projíždět
     */
    private JTextField nextCityTF;

    /***
     * Textové pole s informací o aktuálním množství peněz na autě
     */
    private JTextField moneyTF;

    /***
     * Textové pole s informací o prázdných boxech
     */
    private JTextField emptyTF;

    /***
     * JList aut
     */
    private JList<String> jCarList;

    /***
     * Seznam aut
     */
    private final List<Car> carList = new ArrayList<>();

    /***
     * Vybraný index v JListu
     */
    private int selectedIndex = -1;

    /***
     * Bool proměná říkající jestli došlo ke změně v JListu
     */
    private boolean isListRefreshed = false;

    /**
     * Poslední vybrané auto
     */
    private Car lastSelected;

    /***
     * Vytvoří nové okno s informacemi o autech na základě zadané šířky a výšky
     *
     * @param width  šířka okna
     * @param height výška okna
     */
    public CarFrame(int width, int height){
        super();
        JPanel panel = new JPanel(new BorderLayout());
        this.left = new JPanel();
        this.center = new JPanel();

        panel.setSize(new Dimension(width, height));

        this.add(panel);
        panel.add(left, BorderLayout.WEST);
        panel.add(center, BorderLayout.CENTER);

        this.setMaximumSize(new Dimension(width, height));

        createLeftPanel();
        createRightPanel();

        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setMinimumSize(new Dimension(width, height));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Car info");
        this.pack();
    }

    /***
     * Zobrazí okno
     */
    public void setVisible(){
        this.setVisible(true);
    }

    /***
     * Přidá auto do Jlistu
     *
     * @param car přidávané auto
     */
    public void addCarToList(Car car){
        carList.add(car);

        if(!jCarList.isSelectionEmpty()){
            selectedIndex = jCarList.getSelectedIndex();
            isListRefreshed = true;
        }

        String[] carNames = new String[carList.size()];

        for(int i = 0; i < carNames.length; i++){
            Car car1 = carList.get(i);
            carNames[i] = car1.getType().name() + ": " + i;
        }
        jCarList.setListData(carNames);
    }

    /***
     * Aktualizuje informace o autech
     */
    public void refreshInfo(){
        if(jCarList.getSelectedValue() != null){
            setInfoFields(selectedIndex);
        }

        this.center.repaint();
    }

    /***
     * Vytvoří pravý panel
     */
    private void createRightPanel() {
        GridLayout gl = new GridLayout(7, 2);
        this.center.setLayout(gl);

        JLabel idLabel = new JLabel("ID:");
        JLabel actionLabel = new JLabel("Action:");
        JLabel originLabel = new JLabel("Origin city:");
        JLabel currentLabel = new JLabel("Current city:");
        JLabel nextCityLabel = new JLabel("Next city:");
        JLabel moneyLabel = new JLabel("Money:");
        JLabel emptyBoxesLabel = new JLabel("Empty boxes:");

        idTF = new JTextField();
        actionTF = new JTextField();
        originCityTF = new JTextField();
        currentCityTF = new JTextField();
        nextCityTF = new JTextField();
        moneyTF = new JTextField();
        emptyTF = new JTextField();

        idTF.setEditable(false);
        actionTF.setEditable(false);
        originCityTF.setEditable(false);
        currentCityTF.setEditable(false);
        nextCityTF.setEditable(false);
        moneyTF.setEditable(false);
        emptyTF.setEditable(false);

        this.center.add(idLabel);
        this.center.add(idTF);

        this.center.add(actionLabel);
        this.center.add(actionTF);

        this.center.add(originLabel);
        this.center.add(originCityTF);

        this.center.add(currentLabel);
        this.center.add(currentCityTF);

        this.center.add(nextCityLabel);
        this.center.add(nextCityTF);

        this.center.add(moneyLabel);
        this.center.add(moneyTF);

        this.center.add(emptyBoxesLabel);
        this.center.add(emptyTF);
    }

    /***
     * Vytvoří levý panel
     */
    private void createLeftPanel() {
        this.left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

        JLabel cityLabel = new JLabel("Seznam aut");
        jCarList = new JList<>();

        jCarList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jCarList.setLayoutOrientation(JList.VERTICAL);
        jCarList.setVisibleRowCount(-1);
        jCarList.addListSelectionListener(event -> changeFields());

        cityLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JScrollPane pane = new JScrollPane();
        pane.setViewportView(jCarList);

        this.left.add(cityLabel);
        this.left.add(pane);
    }

    /***
     * Změní informace v textových polích reaguje na změnu prvku v Jlistu
     */
    private void changeFields() {
        if(isListRefreshed){
            jCarList.setSelectedIndex(selectedIndex);
        }else {
            selectedIndex = jCarList.getSelectedIndex();
        }

        setInfoFields(selectedIndex);

        isListRefreshed = false;
    }

    /***
     * Nastaví informace do příslušných textových polích
     *
     * @param id id auta v Jlistu
     */
    private void setInfoFields(int id){
        /***
         * První vybrané auto nebo před tím nebylo vybrané žádné
         */
        if(lastSelected != null){
            lastSelected.setSelected(false);
        }

        /***
         * Nebylo vybráno auto
         */
        if(id == -1){
            idTF.setText("");
            actionTF.setText("");
            originCityTF.setText("");
            currentCityTF.setText("");
            nextCityTF.setText("");
            moneyTF.setText("");
            emptyTF.setText("");
        }else {
            Car car = this.carList.get(id);
            lastSelected = car;
            car.setSelected(true);
            idTF.setText(car.getID()+"");
            actionTF.setText(car.getAction().name());
            originCityTF.setText(((AFinancialInstitute)car.getOriginBank()).getName());
            currentCityTF.setText(car.getCurrentCity().getName());
            nextCityTF.setText(car.getNextCity().getName());
            moneyTF.setText((car.getMoneyOnBoard() * 100_000) + " tolarů (" + car.getMoneyOnBoard() + " boxů)");
            emptyTF.setText(car.getEmptyBoxes() + "");
        }
    }
}
