package cz.zcu.kiv.dispatcher;

import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.nodes.abstracts.AFinancialInstitute;
import cz.zcu.kiv.nodes.interfaces.IGraphNode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class CarUtils {
    /***
     * Získá cestu instancí AFinancialInstitute z cesty specifikované ID bank.
     *
     * @param queue Fronta cesty specifikovaná ID měst
     * @returnFronta cesty specifikovaná instancemi AFinancialInstitute
     */
    public static Queue<IGraphNode> getNodePath(Queue<Integer> queue) {
        Queue<IGraphNode> toReturn = new LinkedList<>();

        while (queue.peek() != null) {
            toReturn.add(Dispatcher.aFinancialInstitutes[queue.remove()]);
        }

        return toReturn;
    }

    /***
     * Aktualizuje souřadnice vozidla. Pokud se nachází mezi dvěma městy, vypočítá souřadnice pomocí poměru času celkové doby cesty a doby cesty od vyjetí.
     */
    public static void updateCarLocation(Car car) {
        if (car.nextCity != null && car.currentCity != null && car.nextAction != CarActions.WAITING && car.nextAction != CarActions.LOADING_DONE && car.nextAction != CarActions.UNLOADING_DONE) {
            float distance = car.fw.pathLengthBetweenCities(car.nextCity.getId(), car.currentCity.getId());
            int distanceTime = calculateDistanceTime(distance, car.type);
            float rate = Math.min(1, (car.currentTime - car.lastCityTime) / (float) distanceTime);

            int pointXOffset = (int) ((car.nextCity.getPoint().x - car.currentCity.getPoint().x) * rate);
            int pointYOffset = (int) ((car.nextCity.getPoint().y - car.currentCity.getPoint().y) * rate);

            Point point = new Point(car.currentCity.getPoint().x + pointXOffset, car.currentCity.getPoint().y + pointYOffset);
            //LOG.logSimulation("Auto ID: " + this.ID + ", Poloha: " + point + ", Původní město: (ID: " + previousCity.getId() + ") " + previousCity.getPoint() + ", Následující město: (ID: " + nextCity.getId() + ") " + nextCity.getPoint() + ", Action: " + this.nextAction + ", Action time: " + this.nextActionTime + ", Time: " + Timer.getTimer().getCurrentTime() + ", Rate: " + rate + ", LastCityTime: " + this.lastCityTime, LogLevel.DEBUG);
            car.location = point;
        } else {
            car.location = car.currentCity.getPoint();
        }
    }

    /***
     * Nalezne další žádosti v městě specifikovaném předanou žádostí, odebere je z fronty a vrátí celkový počet požadovaných boxů
     * @param request Jedna žádost z města
     * @return Počet boxů, o které město žádá
     */
    public static int findOtherRequests(Car car, MoneyRequest request) {
        int toReturn = 0;
        Queue<MoneyRequest> requestsDupe = new LinkedList<>(car.requests);
        Iterator<MoneyRequest> iterator = requestsDupe.iterator();

        while (iterator.hasNext()) {
            MoneyRequest newRequest = iterator.next();
            if (newRequest.getOriginNode() == request.getOriginNode()) {
                toReturn += newRequest.getNumberOfBoxes();

                car.STATS.partialRequest(newRequest);
                car.requests.remove(newRequest);
            }
        }

        return toReturn;
    }

    /***
     * Vypočítá čas v minutách než dorazí do zadaného bodu, do výpočtu je zahrnutá kompletní cesta i zastávky a čas potřebný pro vyložení
     * čas se může změnit, pokud po výpočtu času dorazí žádost do banky, do které už vozidlo jede (Naložení /  vyložení trvá 5 minut)
     *
     * @param destination cílová banka
     * @return čas do dojetí do banky
     */
    public static int getTimeToArrive(Car car, AFinancialInstitute destination) {
        int timeToReturn;
        if (car.nextActionTime == Integer.MAX_VALUE && car.nextAction == CarActions.WAITING) {
            timeToReturn = 0;
        } else {
            timeToReturn = car.nextActionTime - car.currentTime;
        }

        LinkedList<IGraphNode> pathDupe = new LinkedList<>(car.path);
        LinkedList<MoneyRequest> requestsDupe = new LinkedList<>(car.requests);

        //Spočítá čas cesty (Aktuální žádost)
        if (pathDupe.size() >= 2) {
            timeToReturn += calculateDistanceTime(car.fw.pathLengthBetweenCities(pathDupe.getFirst().getId(), pathDupe.getLast().getId()), car.type);
        }

        // Spočítá čas pro všechny žádosti
        MoneyRequest oldReq = null;
        for (MoneyRequest newReq : requestsDupe) {
            if (oldReq != null) {
                timeToReturn += calculateDistanceTime(car.fw.pathLengthBetweenCities(newReq.getOriginNode().getId(), oldReq.getOriginNode().getId()), car.type);
            }
        }

        if (!pathDupe.isEmpty()) {
            timeToReturn += calculateDistanceTime(car.fw.pathLengthBetweenCities(pathDupe.getLast().getId(), destination.getId()), car.type);
        } else {
            timeToReturn += calculateDistanceTime(car.fw.pathLengthBetweenCities(car.currentCity.getId(), destination.getId()), car.type);
        }

        timeToReturn += (car.moneyOnBoard - car.moneyAvailable) * car.TIME_TO_UNLOAD_BOX_AND_LOAD_EMPTY_BOX;

        return timeToReturn;
    }

    /***
     * Vypočítá čas potřebný pro uražení zadaných kilometrů
     *
     * @param distanceToAnother Počet kilometrů, které má vozdilo urazit
     * @param type typ auta
     * @return čas potřebný k uražení v celých minutách
     */
    public static int calculateDistanceTime(double distanceToAnother, CarType type) {
        return (int) (distanceToAnother / type.getTravelSpeed() * 60);
    }
}
