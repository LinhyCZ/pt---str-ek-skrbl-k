package cz.zcu.kiv.nodes.abstracts;

import cz.zcu.kiv.TimeTimer;
import cz.zcu.kiv.data.generating.Point;
import cz.zcu.kiv.dispatcher.Dispatcher;
import cz.zcu.kiv.dispatcher.MoneyRequest;
import cz.zcu.kiv.logging.Log;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.nodes.interfaces.IRequestable;

import java.util.Map;
import java.util.Random;

public abstract class ALocalBank extends AFinancialInstitute implements IRequestable {
    /***
     * Instance loggeru pro zapisování eventů v programu do výstupu
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Random
     */
    private static final Random RND;

    /***
     * Hodiny kdy se může generovat požadavek na box
     */
    private final int REQUEST_HOURS = (TimeTimer.REQUEST_END / 60) + 1;

    /***
     * Upravení křivky Gaussovy funkce tak, aby pokryla všechny časy
     */
    private static final float SIQMA = 1.5f;

    /***
     * Čas, kdy je největší pravděpodobnost, že přijde požadavek na box
     */
    private static final int MAIN_TIME = 11;

    /***
     * Pole pravděpodobností s vygenerováním
     * daného počtu požadavkl na boxy
     */
    private static final int[] PPROBABILITY_REQUEST_ARRAY;

    /**
     * Static block initializes {@code PPROBABILITY_REQUEST_ARRAY}
     * to values 5, 10, 14, 17, 19, 20
     * from which is then picked the number of palettes to request via {@code RND}
     */
    static
    {
        int one = 5 /*-> 5/20 = 0.25*/, two = 10 /*-> 5/20 = 0.25*/, three = 14 /*-> 4/20 = 0.2*/;
        int  four = 17 /*-> 3/20 = 0.15*/, five = 19 /*-> 2/20 = 0.1*/, six = 20 /*-> 1/20 = 0.05*/;
        PPROBABILITY_REQUEST_ARRAY = new int[20];
        for(int i = 0;i < PPROBABILITY_REQUEST_ARRAY.length;i++)
        {
            if(i < one)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 1;
            }
            else if(i < two)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 2;
            }
            else if(i < three)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 3;
            }
            else if(i < four)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 4;
            }
            else if(i < five)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 5;
            }
            else if(i < six)
            {
                PPROBABILITY_REQUEST_ARRAY[i] = 6;
            }
        }
        RND = new Random();
    }

    /***
     * Pole, kdy indexi jsou hodiny a jednotlivé hodnoty v poli je počet boxů
     * které si banka vyžádá v danou hodinu
     */
    private int[] requestTimes;

    public ALocalBank(int id, String name, Point point, Map<Integer, Float> neighbors) {
        super(id, name, point, neighbors);
        requestTimes = new int[REQUEST_HOURS];
    }

    /**
     * Každý den ráno vygeneruje requesty a hodiny v kolik se odešlou
     */
    @Override
    public void initializeRequests() {
        this.requestTimes = generateRequestedTimes();
        StringBuilder hoursAndBoxes = new StringBuilder();
        for(int i = 0; i < requestTimes.length ; i++){
            if(requestTimes[i] > 0){
                hoursAndBoxes.append(String.format("[%dh|%db] ", i, requestTimes[i]));
            }

        }
        String mess = String.format("Rezidence %s si objednává %s", this.name, hoursAndBoxes.toString());
        LOG.logSimulation(mess, LogLevel.DEBUG);
    }

    /**
     * Vygeneruje nový request dispatcherovi, že pobočka potřebuje boxy.
     * Podle toho kolik je jich potřeba tolik se jich vygeneruje.
     */
    @Override
    public void generateRequest(int hour) {
        Dispatcher dispatcher = Dispatcher.getDispatcher();
        if(requestTimes[hour] > 0){
            for(int i = 0; i < requestTimes[hour]; i++){
                dispatcher.newRequest(new MoneyRequest(this));
            }
        }
    }

    /***
     * Metoda vygeneruje podle normálového rozložení, kde indexy poly představují hodiny, kdy
     * rezidence bude chtít vydat požadavek a hodnota v poli, kolik
     * boxů bude rezidence chtít v danou hodinu.
     *
     * @return pole kde indexy představují hodiny a hodnoty na indexech počty boxů
     */
    private int[] generateRequestedTimes(){
        int numberOfPackages = generateNumberOfPackes();
        int startTime = TimeTimer.REQUEST_START / 60;
        int endTime   = TimeTimer.REQUEST_END / 60;

        int[] numberOfPackagesAtTime = new int[endTime+1];
        for(int i = 0; i < numberOfPackages;i++) {
            int result = ((int) Math.round(MAIN_TIME + RND.nextGaussian() * SIQMA));
            if (result < startTime || result > endTime) {
                i--;
                continue;
            }
            numberOfPackagesAtTime[result] += 1;
        }
        return numberOfPackagesAtTime;
    }

    /**
     * Na základě pole s pravděpodobnostma vrátí hodnotu kolik
     * boxů si v tento den vyžádá rezidence
     *
     * @return počet vyžádaných boxů za den
     */
    private int generateNumberOfPackes(){
        return PPROBABILITY_REQUEST_ARRAY[RND.nextInt(20)];
    }

    @Override
    public void addPackages(int packages) {
        //Bez akce
    }

    /**
     * Odešle dispatcherovi reguest o boxy a to je vyšle
     * z nadřazené finační instituce
     *
     * @param packages počet boxů které si instituce žádá
     */
    @Override
    public void sendRequest(int packages) {
        LOG.logSimulation(String.format("Instituce %s zásílá požadavek na %d boxů", this.name, packages), LogLevel.NOTICE);
        MoneyRequest newRequest = new MoneyRequest(this, packages);
        Dispatcher.getDispatcher().newRequest(newRequest);
    }
}
