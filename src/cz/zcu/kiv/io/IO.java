package cz.zcu.kiv.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.zcu.kiv.data.generating.GeneratedNode;
import cz.zcu.kiv.graphing.FloydWarshall;
import cz.zcu.kiv.logging.LogLevel;
import cz.zcu.kiv.logging.Log;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class IO {

    /**
     * Cesta k souboru, kde jsou uložená jména měst
     */
    public static final String CITIES_PATH_NAMES_PATH = "data/mesta.txt";

    /**
     * Cesta k souboru, kde jsou uložená data o jednotlivých institucí
     */
    public static final String CITIES_SOURCES_PATH = "data/tcities.json";

    /***
     * Cesta k souboru, kde jsou uložená data s cestami mezi městy
     */
    public static final String CITIES_PATHS_PATH = "data/tpaths.json";

    /**
     * Instance loggeru pro zaznamenávání událostí
     */
    private static final Log LOG = Log.getLoger();

    /***
     * Jediná instance jedináčka
     */
    private static IO instance = null;

    /***
     * Soukromý konstruktor
     */
    private IO(){}

    /**
     * Vrátí jedinou instanci jedináčka
     *
     * @return instance této třídy
     */
    public static IO getInstance(){
        if(instance == null){
            instance = new IO();
        }
        return instance;
    }

    /***
     * Načte instituce ze souboru.
     *
     * @param path cesta k souboru
     * @return vrátí instuce v poli
     */
    public GeneratedNode[] loadCities(String path){
        LOG.log("Načítám informace o pobočkách ze souboru.", LogLevel.INFO);
        Gson g = new Gson();
        String data = "";
        try{
            BufferedReader bfr = new BufferedReader(new FileReader(path, Charset.forName("UTF-8")));
            data = bfr.readLine();

            bfr.close();
        }catch (IOException ex){
            LOG.log(ex.getMessage(), LogLevel.ERROR);
        }
        GeneratedNode[] cities =  g.fromJson(data, GeneratedNode[].class);
        LOG.log("Načítání informací o pobočkách proběhlo úspěšně.", LogLevel.INFO);
        return cities;
    }

    /***
     * Načte z json souboru se všema uloženýma cestama mezi jednotlivými
     * městy.
     *
     * @param path cesta k souboru
     * @return instance obsahující cesty mezi institucemi
     */
    public FloydWarshall loadPaths(String path){
        Gson g = new Gson();
        String data = "";
        try{
            BufferedReader bfr = new BufferedReader(new FileReader(path));
            data = bfr.readLine();
            bfr.close();
        }catch (IOException ex){
            LOG.log(ex.getMessage(), LogLevel.ERROR);
        }
        FloydWarshall paths =  g.fromJson(data,FloydWarshall.class);
        return paths;
    }

    /***
     * Načte jména měst ze souboru a vrátí je ve frontě stringů
     *
     * @param path cesta k souboru
     * @return  fronta stringů
     */
    public Queue<String> loadCityNames(String path){
        Queue<String> cities = new LinkedList<>();
        try(BufferedReader bfr = new BufferedReader(new FileReader(path, Charset.forName("UTF-8")))){

            int i = 0;
            String line = "";
            while((line = bfr.readLine()) != null && i < 5010){
                cities.add(line);
                i++;
            }
        }catch (IOException ex){
            System.out.println("IO.loadCityNames - IO exception");
        }

        return cities;
    }

    /***
     * Metoda pro uložení institucí ve formátu json
     *
     * @param data  insituce
     * @param name název souboru
     */
    public void saveAsJson(List<GeneratedNode> data, String name)  {
        try (Writer writer = new FileWriter(name )) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(data, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda pro uložení dat s informacemi o všech cestách ve formátu json
     *
     * @param data  data s cestama
     * @param name  název souboru
     */
    public void saveAsJson(FloydWarshall data, String name){
        try (Writer writer = new FileWriter(name)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(data, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Metoda pro zápis do existujícího souboru.
     * V případě že by neexistoval ho vytvoří.
     *
     * @param path cesta k souboru s logem
     * @param text text který se má ukládat
     */
    public void writeToExistingFile(File path, String text) {
        try {
            if(!Files.exists(Log.LOG_DIRECTORY)){
                Files.createDirectory(Log.LOG_DIRECTORY);
            }
            if (!path.exists()) {
                path.createNewFile();
            }

            BufferedWriter out = new BufferedWriter(new FileWriter(path.getPath(), true));
            out.write(text);
            out.newLine();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    }
